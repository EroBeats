#pragma once

#include<vector>
#include<List>
#include<iostream>

#include <SDL/SDL.h>

#include "SFX.h"
#include "BGM.h"

class ResourceMaster
{
public:
	ResourceMaster();
	ResourceMaster(SFX* sfxptr, BGM* bgmptr);
	~ResourceMaster();

	void changeResolution(std::string type);

	std::vector<SDL_Texture*> gameBG;
	std::vector<SDL_Texture*> gameGraphics;
	std::vector< std::vector<SDL_Texture*>> gameAnimations;
	std::vector<Mix_Music*> gameBGM;
	std::vector<Mix_Chunk*> gameSFX;

	std::vector<std::string> gameText;

	SFX* sptr;
	BGM* bptr;

	SDL_Renderer* rendPtr;
	SDL_Window* window;

};

