﻿namespace Language_Editor
{
    partial class LanguageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.Save = new System.Windows.Forms.Button();
            this.japanese = new System.Windows.Forms.Button();
            this.french = new System.Windows.Forms.Button();
            this.english = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.userInput = new System.Windows.Forms.RichTextBox();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Save);
            this.panel2.Controls.Add(this.japanese);
            this.panel2.Controls.Add(this.french);
            this.panel2.Controls.Add(this.english);
            this.panel2.Location = new System.Drawing.Point(788, -1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 528);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(47, 426);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(109, 39);
            this.Save.TabIndex = 3;
            this.Save.Text = "SAVE";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // japanese
            // 
            this.japanese.Location = new System.Drawing.Point(47, 237);
            this.japanese.Name = "japanese";
            this.japanese.Size = new System.Drawing.Size(109, 70);
            this.japanese.TabIndex = 2;
            this.japanese.Text = "JP";
            this.japanese.UseVisualStyleBackColor = true;
            this.japanese.Click += new System.EventHandler(this.japanese_Click);
            // 
            // french
            // 
            this.french.Location = new System.Drawing.Point(47, 141);
            this.french.Name = "french";
            this.french.Size = new System.Drawing.Size(109, 70);
            this.french.TabIndex = 1;
            this.french.Text = "FR";
            this.french.UseVisualStyleBackColor = true;
            this.french.Click += new System.EventHandler(this.french_Click);
            // 
            // english
            // 
            this.english.Location = new System.Drawing.Point(47, 50);
            this.english.Name = "english";
            this.english.Size = new System.Drawing.Size(109, 70);
            this.english.TabIndex = 0;
            this.english.Text = "EN";
            this.english.UseVisualStyleBackColor = true;
            this.english.Click += new System.EventHandler(this.english_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.userInput);
            this.panel1.Location = new System.Drawing.Point(-1, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(783, 528);
            this.panel1.TabIndex = 2;
            // 
            // userInput
            // 
            this.userInput.Location = new System.Drawing.Point(4, 4);
            this.userInput.Name = "userInput";
            this.userInput.Size = new System.Drawing.Size(776, 521);
            this.userInput.TabIndex = 0;
            this.userInput.Text = "";
            // 
            // LanguageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 527);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "Language";
            this.Text = "Language";
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button japanese;
        private System.Windows.Forms.Button french;
        private System.Windows.Forms.Button english;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.RichTextBox userInput;
    }
}

