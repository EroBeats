#pragma once

#include "FileIO.h"
#include "ResourceMaster.h"
#include "Fonts.h"
#include "Animation.h"
#include "SFX.h"
#include "ScreenTemplate.h"

#include <chrono>
#include <SDL\SDL.h>
class ScreenTemplate
{
public:
	ScreenTemplate();
	~ScreenTemplate();

	void gameLoop();
	bool fadeIn(double durration, SDL_Texture* fadeTexture);
	bool fadeOut(double durration, SDL_Texture* fadeTexture);
	void fadeOutMusic(int fadeOutDuration);

	virtual void processInput();
	virtual void update();
	virtual void play();
	virtual void render();
	virtual void close();

	int x;
	int y;
	int alpha;
	double fadeRate;
	double FPSDelay;

	int* screen;
	Fonts* font;
	SDL_Renderer* renderer;
	ResourceMaster* rsc;
	FileIO* fio;

	bool mouseClicked;
	bool mouseClickedL;
	bool mouseClickedR;
	bool running;
	bool playedOnce;
	bool playedOnceL;

};

