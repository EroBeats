#pragma once

#include <SDL/SDL.h>
#include <SDL Image/SDL_image.h>

#define GLEW_BUILD
#include <GL/glew.h>
#include <Windows.h>

#include<vector>
#include<List>

#include<iostream>

#include "ResourceMaster.h"
#include "SplashScreen.h"
#include "FileIO.h"

class SDLGraphics
{
public:

	//static method
	void SDLGraphics::setupGraphics(FileIO* filePntr)
	{
		loadResources(filePntr);
		loaded = true;
	}

	SDLGraphics(FileIO* filePntr, ResourceMaster* rscPointer);
	~SDLGraphics();

	void createWindow();
	void startThread();
	void createLoadScreen();
	void setupGame();

	SDL_Window* window;
	ResourceMaster* rsc;
	FileIO* fio;

	SDL_Renderer* renderer;

	int screenWidth;
	int screenHeight;

	bool loaded;

private:
	void createPointers();
	void loadResources(FileIO* fio);

};

