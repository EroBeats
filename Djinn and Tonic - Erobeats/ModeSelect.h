#pragma once
#include "ScreenTemplate.h"
class ModeSelect :
	public ScreenTemplate
{
public:
	ModeSelect(int* screen,FileIO* fileIO, ResourceMaster* resourcePointer);
	~ModeSelect();

	void update();
	void play();
	void render();
	void close();

	void destroyFonts();
	void createFonts();


	bool getSkip();
	int getGameMode();
	std::array<Animation*, 2> getAnimations();

	bool hoverArcade;
	bool hoverLife;
	bool hoverStart;
	bool modeSelected;

	bool* hoverAddress;
	bool* hoverAddressOld;

	std::string  highscore;
	std::string highlife;

	SDL_Texture* textArcade;
	SDL_Rect rectArcade;

	SDL_Texture* textLife;
	SDL_Rect rectLife;
	
	SDL_Texture* textHighScore;
	SDL_Rect rectHighScore;

	SDL_Texture* textHighLife;
	SDL_Rect rectHighLife;

	SDL_Texture* textGoal;
	SDL_Rect rectGoal;

	SDL_Texture* textSubGoal;
	SDL_Rect rectSubGoal;

	SDL_Texture* textStart;
	SDL_Rect rectStart;

	Animation *outlineAnimation;
	std::array <Animation*, 2> animations;

private:

	int gameMode;
	bool skip;
};

