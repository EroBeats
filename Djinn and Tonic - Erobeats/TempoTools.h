#pragma once

#include "RhythmIO.h"

#include <vector>
#include <chrono>
#include <iostream>

class TempoTools
{
public:
	TempoTools(RhythmIO* rio);
	~TempoTools();

	short autoPlay(int type);
	short clickAccuracy(int type, bool cout);
	short clickAccuracyRange(int type);
	double clickTempo(int time);
	void moveTicker();

	void createTimeData(std::string levelPath);
	void clearTimeData();
	void proccessDataSlow();
	void setMusicStart(long long musicStart);	
	void countDown(float* time, float durration);

	long long musicStart;

	long long fileTime[9];
	long long oldTime[9];
	long long cueFileTime[9];

	bool autoplayed[9];

	RhythmIO* rio;

	std::string levelPath;

	bool done;
	double tempo;

	int errorMarginPerf;
	int errorMarginGood;
	int errorMarginOK;
	int errorMarginBad;

	std::vector<double> clickTimes;
	std::vector<std::vector<int>> timeKeys;
	std::vector<std::vector<long long>> hitData;
	std::vector<std::vector<long long>> cueData;

private:

};

