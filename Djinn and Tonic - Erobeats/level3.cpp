#include "Level3.h"
#include "rescourceKeys.h"
Level3::Level3(int gameType, FileIO* fileIO, ResourceMaster* resourcePointer, std::array<Animation*, 2> animations)
{
	
	GT = gameType;
	score = 0;
	heartCount = 3;
	rsc = resourcePointer;
	renderer = rsc->rendPtr;

	fio = fileIO;
	rio = new RhythmIO(fio);
	tTools = new TempoTools(rio);
	font = new Fonts(0, rsc);

	goodL = new Animation(rsc->gameAnimations.at(L3Animation1OK));
	badL = new Animation(rsc->gameAnimations.at(L3Animation1Bad));

	pump1R = new Animation(rsc->gameAnimations.at(L3Animation2_1));
	pump2R = new Animation(rsc->gameAnimations.at(L3Animation2_2));
	pump3R = new Animation(rsc->gameAnimations.at(L3Animation2_3));

	/******************/
	pump0R_R = animations[0];
	pump1R_R = animations[1];
	pump2R_R = new Animation(fio->loadSpecificZip("1_2.zip"));
	pump3R_R = new Animation(fio->loadSpecificZip("1_3.zip"));
	/******************/

	release1 = new Animation(rsc->gameAnimations.at(L3Animation3_1));
	release2 = new Animation(rsc->gameAnimations.at(L3Animation3_2));
	release3 = new Animation(rsc->gameAnimations.at(L3Animation3_3));
	release4 = new Animation(rsc->gameAnimations.at(L3Animation3_4));

	int heartW = font->prcnt(0.2, 'x');
	int heartH = font->prcnt(0.3, 'y');
	mainBox = {font->prcnt(0, 'x'), font->prcnt(0, 'y'),  font->prcnt(0.8, 'x'), font->prcnt(1, 'y')};
	rectHeart1 = { font->prcnt(0.8, 'x'), font->prcnt(0.05, 'y'), heartW, heartH, };
	rectHeart2 = { font->prcnt(0.8, 'x'), font->prcnt(0.35, 'y'), heartW, heartH, };
	rectHeart3 = { font->prcnt(0.8, 'x'), font->prcnt(0.65, 'y'), heartW, heartH, };

	rectScoreTitle = { font->prcnt(0.8, 'x'), font->prcnt(0.35, 'y'), heartW, heartH, };
	
	font->loadFont(SDL_Color{100,100,100,00}, rsc->gameText.at(ScoreText), 0);
	textScoreTitle = font->getTexture();
	rectScoreTitle = { font->prcnt(0.8, 'x'), font->prcnt(0.5, 'y'), 100, 100 };
	rectScore = { font->prcnt(0.8, 'x'), font->prcnt(0.35, 'y'), 100, 100 };


	/******************/
	initializeBeats("Levels/Test_10Sec");
	/******************/
	pumpCounter = 0;
	playSpeed = 30;
	doneFade = false;
	alpha = 255;
	//EDITOR NEEDS 10'S FOR CUES

	//START MUSIC ALSO STARTS CLOCK
	startMusic(Song3, 1000);

	gameLoop();
}


Level3::~Level3()
{
	delete rio;
	delete tTools;
	delete font;
}

void Level3::update()
{
	if (!tTools->done) {
		tTools->proccessDataSlow();
		tTools->moveTicker();
		playCue[0] = tTools->autoPlay(10);
		playCue[1] = tTools->autoPlay(20);
		playCue[2] = tTools->autoPlay(30);
		playCue[3] = tTools->autoPlay(40);
		if (pumpCounter > 0 && playedOnce2) {
			pumpCounter--;
			//AKA if interupted
			playAniR = false;
			playedOnce4 = false;
			playSound[1] = 9;
		}

		if (playCue[3] == 1) {
			playAniPull = true;
		}
		if (playCue[2] == 1) {
			playAniRelease = true;
			playAniPull = false;
		}
		if (playCue[0] == 1 || playCue[0] == 1) {
			playAniRelease = false;
		}
	}
	else {
		//AKA hard close
		//std::cout << "TRACK DONE\n";
		fadeOutMusic(100);
	}

	if (heartCount > 0) {
		if (leftMouseClicked && !heldL) {
			playSound[0] = tTools->clickAccuracyRange(1);
			//clicked while playing
			if (playAniLGood) {
				playAniLGood = false;
				playAniLBad = true;
				goodL->done = true;
				playedOnce3 = false;
				badL->currentPosition = goodL->currentPosition;
				goodL->resetAnimation();

				if (GT == 1)score -= 100;
				if (GT == 2) heartCount--;
			}
			//clicked while playing bad
			else if (playAniLBad) {
				goodL->done = true;
				playedOnce3 = false;

				if (GT == 1)score -= 100;
				if (GT == 2) heartCount--;
			}
			//clicked badly
			else if (playSound[0] == 3) {
				playAniLBad = true;
				badL->done = false;

				if(GT == 1)	score -= 100;
				if(GT == 2) heartCount--;
			}
			else if (playSound[0] == 2 && !playAniLGood) {
				if(GT == 1)	score += 100;
				playAniLGood = true;
			}
			else if (playSound[0] == 1 && !playAniLGood) {
				if (GT == 1)	score += 200;
				playAniLGood = true;
			}
			else if (playSound[0] == 0 && !playAniLGood) {
				if (GT == 1)	score += 300;
				playAniLGood = true;
			}
		}
		if (rightMouseClicked && !heldR) {
			playSound[1] = tTools->clickAccuracy(2, true);
			//AKA Clicking repeatedly late
			if (playSound[1] != 0 || playAniR) {
				playedOnce4 = false;
				if (GT == 1) score -= 500;
				if (GT == 2) heartCount--;
			}
			//AKA Ani took too long or clicking repeatedly on cue
			else if (playSound[1] == 0 && playAniR) {
				playAniR = false;
				playedOnce4 = false;
				pump1R->resetAnimation();
				pump2R->resetAnimation();
				pump3R->resetAnimation();
			}
			else if (playSound[1] == 0 && !playAniR) {
				playAniR = true;
				playedOnce4 = false;
				score += 500;
				pumpCounter++;
				if (pumpCounter > 3) pumpCounter = 3;
			}
		}
	}
	else {
		//screenfade with no control loss, calls for running to end after 5 seconds of fade. Possible Bgm change
		softClose();
	}

}


void Level3::play()
{

	//CUES
	if (playCue[0] == 1) {
		//running = false;
	}

	if (playCue[0] == 0) {
		playedOnce = false;
	}
	else if (playCue[0] == 1 && !playedOnce) {
		rsc->sptr->playSFX(LeftSFX);
		playedOnce = true;
	}
	if (playCue[1] == 0) {
		playedOnce1 = false;
	}
	else if (playCue[1] == 1 && !playedOnce1) {
		//std::cout << "R\n";
		rsc->sptr->playSFX(RightSFX);
		playedOnce1 = true;
	}
	if (playCue[2] == 0) {
		playedOnce2 = false;
	}
	else if (playCue[2] == 1 && !playedOnce2) {
		rsc->sptr->playSFX(ReleaseSFX);
		playedOnce2 = true;
	}
	if (playCue[3] == 0 && !playedOnce2 && playedOnce2_1) {}
	else if (playCue[3] == 0) {
		playedOnce2_1 = false;
	}
	else if (playCue[3] == 1 && !playedOnce2_1) {
		rsc->sptr->playSFX(BadSFX);
		playedOnce2_1 = true;
	}

	//HITS
	if (!playedOnce3) {
		if (playSound[0] == 0) {
			rsc->sptr->playSFX(PerfectSFX);
			/*goodL->resetAnimation();*/
			playedOnce3 = true;
		}
		else if (playSound[0] == 1) {
			rsc->sptr->playSFX(GoodSFX);
			/*goodL->resetAnimation();*/
			playedOnce3 = true;
		}
		else if (playSound[0] == 2) {
			rsc->sptr->playSFX(OKSFX);
			/*goodL->resetAnimation();*/
			playedOnce3 = true;
		}
		else if (playSound[0] == 3 && (playAniR || tTools->clickAccuracy(2, false) == 0)) {
			rsc->sptr->playSFX(ErrSFX);
			playSound[0] = 9;
			playedOnce3 = false;
			playAniLBad = false;
		}
		else if (playSound[0] == 3) {
			rsc->sptr->playSFX(BadSFX);
			/*badL->resetAnimation();*/
			if (GT == 2 || goodL->done) {
				rsc->sptr->playSFX(ErrSFX);
			}
			playedOnce3 = true;
		}
	}
	
	if (!playedOnce4) {
		if (playSound[1] == 1 || playSound[1] == -1) {
			if (GT == 2) {
				rsc->sptr->playSFX(ErrSFX);
				playedOnce4 = true;
				playSound[1] = 9;
			}
			else {
				rsc->sptr->playSFX(ErrSFX);
				playedOnce4 = true;
				playSound[1] = 9;
			}
		}
		else if (playSound[1] == 0) {
			rsc->sptr->playSFX(PumpSFX);
			playedOnce4 = true;
		}
	}
}

void Level3::render()
{
	SDL_RenderClear(renderer);

	SDL_RenderCopy(renderer, rsc->gameBG.at(Level3BG), NULL, &mainBox);

	//Invisi: playedOnce4 not turning off
	//HITS
	if (playAniLGood && !playAniR) {
		SDL_RenderCopy(renderer, goodL->itterateAnimation(playSpeed), NULL, &mainBox);
		if (goodL->done) {
			playAniLGood = false;
			playedOnce3 = false;
			goodL->done = false;
			badL->done = false;
			playSound[0] = 9;
		}
	}
	else if (playAniLBad && !playAniR) {
		SDL_RenderCopy(renderer, badL->itterateAnimation(playSpeed), NULL, &mainBox);
		if (badL->done) {
			playAniLBad = false;
			playedOnce3 = false;
			badL->done = false;
			goodL->done = false;
			playSound[0] = 9;
		}
	}
	else if (playAniR) {
		if (pumpCounter == 1) {
			SDL_RenderCopy(renderer, pump1R->itterateAnimation(20), NULL, &mainBox);
			if (pump1R->done) {
				playedOnce4 = false;
				playAniR = false;
				playSound[1] = 9;
			}
		}
		else if (pumpCounter == 2) {
			SDL_RenderCopy(renderer, pump2R->itterateAnimation(20), NULL, &mainBox);
			if (pump2R->done) {
				playedOnce4 = false;
				playAniR = false;
				playSound[1] = 9;
			}
		}
		else if (pumpCounter == 3) {
			SDL_RenderCopy(renderer, pump3R->itterateAnimation(20), NULL, &mainBox);
			if (pump3R->done) {
				playedOnce4 = false;
				playAniR = false;
				playSound[1] = 9;
			}
		}
		else std::cout << "PUMPCOUNT ERR: #" << pumpCounter << "\n";
	}
	//CUES
	else if (playAniPull) {
		if (pumpCounter == 3) {
			if (!pump3R_R->done) {
				SDL_RenderCopy(renderer, pump3R_R->itterateAnimation(20), NULL, &mainBox);
			}
			else {
				SDL_RenderCopy(renderer, pump3R_R->frames.at(4), NULL, &mainBox);
			}
		}
		else if (pumpCounter == 2) {
			if (!pump2R_R->done) {
				SDL_RenderCopy(renderer, pump2R_R->itterateAnimation(20), NULL, &mainBox);
			}
			else {
				SDL_RenderCopy(renderer, pump2R_R->frames.at(4), NULL, &mainBox);
			}
		}
		else if (pumpCounter == 1) {
			if (!pump1R_R->done) {
				SDL_RenderCopy(renderer, pump1R_R->itterateAnimation(20), NULL, &mainBox);
			}
			else {
				SDL_RenderCopy(renderer, pump1R_R->frames.at(4), NULL, &mainBox);
			}
		}
		else {
			if (!pump0R_R->done) {
				SDL_RenderCopy(renderer, pump0R_R->itterateAnimation(20), NULL, &mainBox);
			}
			else {
				SDL_RenderCopy(renderer, pump0R_R->frames.at(4), NULL, &mainBox);
			}
		}
	}
	else if (playAniRelease) {
		if (pumpCounter > 2) {
			SDL_RenderCopy(renderer, release1->itterateAnimation(20), NULL, &mainBox);
		}
		else if (pumpCounter > 1) {
			SDL_RenderCopy(renderer, release2->itterateAnimation(20), NULL, &mainBox);
		}
		else if (pumpCounter > 0) {
			SDL_RenderCopy(renderer, release3->itterateAnimation(20), NULL, &mainBox);
		}
		else {
			SDL_RenderCopy(renderer, release4->itterateAnimation(20), NULL, &mainBox);
		}
	}
	//STATIC FREEZE
	else if (pump1R->done && pumpCounter == 1) {
		SDL_RenderCopy(renderer, pump1R->frames.at(9), NULL, &mainBox);
	}
	else if (pump2R->done && pumpCounter == 2){
		SDL_RenderCopy(renderer, pump2R->frames.at(9), NULL, &mainBox);
		pump1R->checkDone();
	}
	else if (pump3R->done && pumpCounter == 3){
		SDL_RenderCopy(renderer, pump3R->frames.at(9), NULL, &mainBox);
		pump2R->checkDone();
	}
	else {
		SDL_RenderCopy(renderer, rsc->gameAnimations.at(L3Animation1OK).at(1), NULL, &mainBox);
		pump3R->checkDone();

		/*
		playedOnce = false;
		playedOnce1 = false;
		playedOnce2 = false;
		playedOnce2_1 = false;
		playedOnce3 = false;
		playedOnce4 = false;
		loop = false;*/
		pump3R_R->done = false;
		pump2R_R->done = false;
		pump1R_R->done = false;
		pump0R_R->done = false;
	}

	if (GT == 1) {
		font->loadFont(SDL_Color{ 100,100,100,00 }, std::to_string(score), 0);
		SDL_Texture* textScore = font->getTexture();
		SDL_RenderCopy(renderer, textScore, NULL, &rectScore);
		SDL_RenderCopy(renderer, textScoreTitle, NULL, &rectScoreTitle);

		SDL_DestroyTexture(textScore);
	}
	else if (GT == 2) {
		/******************/
		if(heartCount == 0) SDL_RenderCopy(renderer, rsc->gameGraphics.at(GameOver), NULL, &mainBox);
		/******************/
		if (heartCount > 0) SDL_RenderCopy(renderer, rsc->gameGraphics.at(Heart), NULL, &rectHeart1);
		if (heartCount > 1) SDL_RenderCopy(renderer, rsc->gameGraphics.at(Heart), NULL, &rectHeart2);
		if (heartCount > 2) SDL_RenderCopy(renderer, rsc->gameGraphics.at(Heart), NULL, &rectHeart3);
	}

	//BUFFER SCREENS
	if (!doneFade && !tTools->done) {
		std::cout << alpha << "Texture fading out - Music Fades in from Constructor\n";
		doneFade = fadeOut(1.0, rsc->gameGraphics.at(LangBGFake));
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(LangBGFake), NULL, NULL);
	}
	else if (tTools->done) {
		std::cout << alpha << "Texture fading in - Music Fading out in Update\n";
		doneFade = fadeIn(2.0, rsc->gameGraphics.at(LangBGFake));
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(LangBGFake), NULL, NULL);	
		if (doneFade) {
			running = false;
		}
	}

	SDL_RenderPresent(renderer);
}

void Level3::close() {
	pump0R_R->destroyAnimation();
	pump1R_R->destroyAnimation();
	pump2R_R->destroyAnimation();
	pump3R_R->destroyAnimation();
	SDL_DestroyTexture(textScoreTitle);
}
void Level3::softClose() {

}

int Level3::getScore() {
	return score;
}
int Level3::getHearts() {
	return heartCount;
}
