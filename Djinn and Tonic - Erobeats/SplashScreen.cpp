#include "SplashScreen.h"

#include "RescourceKeys.h"

SplashScreen::SplashScreen(bool* rscLoaded, ResourceMaster* rsc)
{
	loaded = rscLoaded;
	do{
		SDL_RenderClear(rsc->rendPtr);
		SDL_RenderCopy(rsc->rendPtr, rsc->gameBG.at(LogoBG), NULL, NULL);
		SDL_RenderPresent(rsc->rendPtr);

		SDL_Event evnt;
		while (SDL_PollEvent(&evnt)) {
			SDL_PumpEvents();
			switch (evnt.type) {
			case SDL_QUIT:
				exit(0);
				break;
			}
		}
	} while (!*loaded);
}


SplashScreen::~SplashScreen()
{
}
