#include "Trainer.h"

#include "ScreenNames.h"
#include "RescourceKeys.h"
using namespace std::chrono;
Trainer::Trainer()
{
}

Trainer::Trainer(int* scrn, FileIO* fileIO, ResourceMaster* resourcePointer, int pattern)
{
	rsc = resourcePointer;
	screen = scrn;
	rio = new RhythmIO(fileIO);
	tTools = new TempoTools(rio);
	font = new Fonts(0, rsc);
	renderer = rsc->rendPtr;

	outlineAnimation = new Animation(rsc->gameAnimations.at(WhiteBlueBG));

	lightNumber = 0;

	fadeOutMusic(500);
	SDL_Delay(500);

	countDownTime = 10;
	countDownDuration = 300;

	switch (pattern) {
	case 11: 
		isPattern1_1 = true;
		initializeBeats("Levels/T1");
		break;
	case 12:
		isPattern1_2 = true;
		initializeBeats("Levels/T2");
		break;
	}

	createText();

	int col1 = font->prcnt(0.1, 'x');
	int col2 = font->prcnt(0.2, 'x');
	int col3 = font->prcnt(0.3, 'x');
	int col4 = font->prcnt(0.5, 'x');

	goalRect = { col1, font->prcnt(0.1, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };
	subGoalRect = { col2, font->prcnt(0.15, 'y'), font->prcnt(0.05, 'x') , font->prcnt(0.05, 'y') };
	levelRect = { col3, font->prcnt(0.2, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };

	nextRect = { col4, font->prcnt(0.3, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };
	startRect = { col4, font->prcnt(0.3, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };

	countDownRect = { col2, font->prcnt(0.3, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };

	aheadRect = { col3, font->prcnt(0.8, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };
	behindRect = { col1, font->prcnt(0.8, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };
	perfectRect = { col2, font->prcnt(0.8, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };

	light1Rect = { col2, font->prcnt(0.7, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };
	light2Rect = { col3, font->prcnt(0.7, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };
	light3Rect = { col4, font->prcnt(0.7, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };
	light4Rect = { col4, font->prcnt(0.7, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };


	mouseRect = { col4, font->prcnt(0.1, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };
	mouseRightRect = { col4, font->prcnt(0.5, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };
	mouseLeftRect = { col4, font->prcnt(0.5, 'y'), font->prcnt(0.1, 'x') , font->prcnt(0.1, 'y') };

	startMusic(-1, 0);
	gameLoop();
}


Trainer::~Trainer()
{
	delete rio;
	delete tTools;
	delete font;
	destroyText();
}

void Trainer::createText() {
	if (isPattern1_1) {
		font->loadFont(SDL_Color{ 0,0,0,0 }, rsc->gameText.at(GoalText), 2);
		goalText = font->getTexture();
		font->loadFont(SDL_Color{ 0,0,0,0 }, rsc->gameText.at(SubGoalText), 2);
		subGoalText = font->getTexture();
		font->loadFont(SDL_Color{ 0,0,0,0 }, rsc->gameText.at(Animation1Text), 2);
		levelText = font->getTexture();

		font->loadFont(SDL_Color{ 0,0,0,0 }, rsc->gameText.at(NextText), 2);
		nextText = font->getTexture();

		font->loadFont(SDL_Color{ 0,0,0,0 }, to_string(countDownTime), 2);
		countDownText = font->getTexture();

	}
	else if (isPattern1_2) {
		font->loadFont(SDL_Color{ 0,0,0,0 }, rsc->gameText.at(GoalText), 2);
		goalText = font->getTexture();
		font->loadFont(SDL_Color{ 0,0,0,0 }, rsc->gameText.at(SubGoalText), 2);
		subGoalText = font->getTexture();
		font->loadFont(SDL_Color{ 0,0,0,0 }, rsc->gameText.at(Animation1Text), 2);
		levelText = font->getTexture();

		font->loadFont(SDL_Color{ 0,0,0,0 }, rsc->gameText.at(StartText), 2);
		startText = font->getTexture();

		font->loadFont(SDL_Color{ 0,0,0,0 }, to_string(countDownTime), 2);
		countDownText = font->getTexture();
	}
}

void Trainer::destroyText() {
	SDL_DestroyTexture(goalText);
	SDL_DestroyTexture(subGoalText);
	SDL_DestroyTexture(levelText);
	SDL_DestroyTexture(startText);
	SDL_DestroyTexture(nextText);
	SDL_DestroyTexture(countDownText);
}

void Trainer::loadPattern() {
	if (isPattern1_1) {
		initializeBeats("Levels/T1");
		startMusic(-1, 0);
	}
	else if (isPattern1_2) {
		initializeBeats("Levels/T2");
		startMusic(-1, 0);
	}
}

void Trainer::update() {
	if (!tTools->done) {
		if (isPattern1_2 && x > startRect.x && x < startRect.x + startRect.w  && y > startRect.y && y < startRect.y + startRect.h) {
			hoverStart = true;
		}
		else {
			hoverStart = false;
		}
		if (hoverStart && mouseClicked && !(playedOnce1 || playedOnce2)) {
			start = true;
		}
		if (start && isPattern1_2) {
			destroyText();
			running = false;
		}

		if (isPattern1_1 && x > nextRect.x && x < nextRect.x + nextRect.w  && y > nextRect.y && y < nextRect.y + nextRect.h) {
			hoverNext = true;
		}
		else {
			hoverNext = false;
		}
		if (hoverNext && mouseClicked && !(playedOnce1 || playedOnce2)) {
			next = true;
		}
		if (next && isPattern1_1) {
			isPattern1_1 = false;
			isPattern1_2 = true;
			next = false;
			destroyText();
			createText();
			loadPattern();
		}

		tTools->countDown(&countDownTime, countDownDuration);
		if (countDownTime < 0) {
			exit(1);
		}
		tTools->proccessDataSlow();
		tTools->moveTicker();
		playCue[0] = tTools->autoPlay(10);
		playCue[1] = tTools->autoPlay(20);
		playCue[1] = tTools->autoPlay(30);

		if (isPattern1_1) {
			if (playCue[0] == 1) {
				if (light2Switch) {
					light2Switch = false;
				}
				else {
					if (light1Switch)
						light2Switch = true;
					light1Switch = true;
					light3Switch = false;
				}
				perfect = false;
				behind = false;
				ahead = false;
			}
		}

		if (leftMBSwitch && playCue[0] == 1) {
			leftMBSwitch = false;
		}
		else if (playCue[1] == 1) {
			leftMBSwitch = true;
		}
		if (rightMBSwitch && playCue[2] == 1) {
			rightMBSwitch = false;
		}
		else if (playCue[2]== 1) {
			rightMBSwitch = true;
		}

		/*
		playSound1 = tTools->clickAccuracy(1);
		std::cout << playSound1 << "\n";
		if (playSound1 == 0) {
		perfect = true;
		behind = false;
		ahead = false;
		}
		else if (playSound1 == 1) {
		behind = true;
		ahead = false;
		perfect = false;
		}
		else if (playSound1 == -1) {
		ahead = true;
		perfect = false;
		behind = false;
		}
		*/

		if (leftMouseClicked && !playedOnceL) {
			if (isPattern1_1) {
				playSound[0] = tTools->clickAccuracy(1, true);
				if (playSound[0] == 0) {
					light1Switch = false;
					light2Switch = false;
					light3Switch = true;
					perfect = true;
					behind = false;
					ahead = false;
				}
			}
			if (playSound[0] == 1) {
				behind = true;
				ahead = false;
				perfect = false;
			}
			else if (playSound[0] == -1) {
				ahead = true;
				perfect = false;
				behind = false;
			}
		}
	}
	else {
		running = false;
	}

}
void Trainer::play() {
	if (playCue[0] == 1 ) {
		rsc->sptr->playSFX(2);
		playCue[0] = 0;
	}
	if (playSound[0] == 0 && !playedOnceL) {
		rsc->sptr->playSFX(3);
		playSound[0] = 9;
	}
	else if (playSound[0] == 1 && !playedOnceL) {
		rsc->sptr->playSFX(1);
		playSound[0] = 9;
	}
	else if (playSound[0] == -1 && !playedOnceL) {
		rsc->sptr->playSFX(1);
		playSound[0] = 9;
	}
}

void Trainer::render() {
	SDL_RenderClear(renderer);

	if (hoverStart || hoverNext) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &startRect);
	}

	SDL_RenderCopy(renderer, goalText, NULL, &goalRect);
	SDL_RenderCopy(renderer, subGoalText, NULL, &subGoalRect);
	SDL_RenderCopy(renderer, levelText, NULL, &levelRect);
	SDL_RenderCopy(renderer, startText, NULL, &startRect);
	SDL_RenderCopy(renderer, nextText, NULL, &nextRect);

	stringstream stream;
	stream << fixed << setprecision(3) << countDownTime;
	string formattedCounter = stream.str();

	font->loadFont(SDL_Color{ 0,0,0,0 }, formattedCounter, 2);
	countDownText = font->getTexture();

	SDL_RenderCopy(renderer, countDownText, NULL, &countDownRect);

	SDL_DestroyTexture(countDownText);

	//generate lights
	SDL_RenderCopy(renderer, rsc->gameGraphics.at(RedDark), NULL, &light1Rect);
	SDL_RenderCopy(renderer, rsc->gameGraphics.at(RedDark), NULL, &light2Rect);
	SDL_RenderCopy(renderer, rsc->gameGraphics.at(GreenDark), NULL, &light3Rect);

	if (light1Switch) {
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(RedBright), NULL, &light1Rect);
	}
	if (light2Switch) {
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(RedBright), NULL, &light2Rect);
	}
	if (light3Switch) {
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(GreenBright), NULL, &light3Rect);
	}

	//generate mouse
	SDL_RenderCopy(renderer, rsc->gameGraphics.at(Mouse), NULL, &mouseRect);

	if (leftMBSwitch) {
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(MouseL), NULL, &mouseLeftRect);
	}
	if (rightMBSwitch) {
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(MouseR), NULL, &mouseRightRect);
	}

	//gen arrows
	if (behind) {
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(LArrow), NULL, &aheadRect);
	}
	else if (ahead) {
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(RArrow), NULL, &behindRect);
	}
	else if(perfect) {
		SDL_RenderCopy(renderer, rsc->gameGraphics.at(Circle), NULL, &perfectRect);
	}

	SDL_RenderPresent(renderer);
}