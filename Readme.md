# Djinn and Tonic
* The 'first' game I worked on.
* Stopped in 2016 sometime.
* Did all the graphics myself and wasn't really sure what I was doing at the time in terms of code. 
* Artwork is poor, got tired of doing the art myself realizing I knew even less of what I was doing in terms of art.
* There are some salvagable pieces of code in here that I find interesting such as working with zlib to create encrypted data of the game files
* Has a beatmap editor, langauge editor and game files
* Made with SDL2
 