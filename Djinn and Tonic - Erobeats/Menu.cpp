#include "menu.h"
#include "ScreenNames.h"
#include "RescourceKeys.h"


//create the animations, load the text and then play the music

Menu::Menu(int* scrs, FileIO* filePntr, ResourceMaster* resourcePointer)
{
	hoverLevelSel = false;
	hoversettings = false;
	hovermusicRoom = false;
	hoverQuit = false;

	rsc = resourcePointer;

	renderer = rsc->rendPtr;
	screen = scrs;

	outlineAnimation = new Animation(rsc->gameAnimations.at(WhiteBlueBG));

	font = new Fonts(0, rsc);

	int textSizeX = font->prcnt(0.20, 'x');
	int textSizeY = font->prcnt(0.15, 'y');
	int textAlignX = font->prcnt(0.75, 'x');

	font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(LevelSelectText), 2);
	textLevelSelect = font->getTexture();
	rectLevelSelect = {textAlignX, font->prcnt(0.05, 'y'), textSizeX , textSizeY };

	font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(SettingsText), 2);
	textSettings = font->getTexture();
	rectSettings = { textAlignX, font->prcnt(0.25, 'y'), textSizeX , textSizeY };

	font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(MusicRoomText), 2);
	textMusicRoom = font->getTexture();
	rectMusicRoom = { textAlignX, font->prcnt(0.45, 'y'), textSizeX , textSizeY };

	font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(QuitText), 2);
	textQuit = font->getTexture();
	rectQuit = { textAlignX, font->prcnt(0.65, 'y'), textSizeX , textSizeY };

	rsc->bptr->playBGM();
	gameLoop();
}

Menu::~Menu() {

}

void Menu::close()
{
	delete font;
	delete outlineAnimation;
	destroyFonts();
}

void Menu::destroyFonts() {
	SDL_DestroyTexture(textLevelSelect);
	SDL_DestroyTexture(textSettings);
	SDL_DestroyTexture(textMusicRoom);
	SDL_DestroyTexture(textQuit);
}

void Menu::update()
{
	hoverLevelSel = false;
	hoversettings = false;
	hovermusicRoom = false;
	hoverQuit = false;

	if (x > rectQuit.x && y > rectQuit.y) {
		//cout << "ovr4\n";
		hoverQuit = true;
	}
	else if (x > rectMusicRoom.x && y > rectMusicRoom.y) {
		//cout << "ovr3\n";
		hovermusicRoom = true;
	}
	else if (x > rectSettings.x && y > rectSettings.y) {
		//cout << "ovr2\n";
		hoversettings = true;
	}

	else if (x > rectLevelSelect.x  && y > rectLevelSelect.y) {
		//cout << "ovr1\n";
		hoverLevelSel = true;
	}
	else {
		////cout << "notOvr\n";
		hoverLevelSel = false;
		hoversettings = false;
		hovermusicRoom = false;
		hoverQuit = false;
	}

	if (mouseClicked && hoverLevelSel) {
		//cout << "Fired1\n";
		running = false;
		*screen = LevelSelectScreen;
	}
	else if (mouseClicked && hoversettings) {
		//cout << "Fired2\n";
		running = false;
		*screen = SettingsScreen;
	}
	else if (mouseClicked && hovermusicRoom) {
		running = false;
		*screen = MusicRoomScreen;
	}
	else if (mouseClicked && hoverQuit) {
		running = false;
		*screen = Quit;
	}
}

void Menu::play()
{
	if (hoverLevelSel) {
		hoverAddress = &hoverLevelSel;
	}
	else if (hoversettings) {
		hoverAddress = &hoversettings;
	}
	else if (hovermusicRoom) {
		hoverAddress = &hovermusicRoom;
	}
	else if (hoverQuit) {
		hoverAddress = &hoverQuit;
	}

	if (mouseClicked && (hoverLevelSel || hoversettings || hovermusicRoom || hoverQuit)) {
		rsc->sptr->playSFX(0);
	}
	
	if (!playedOnce && (hoverLevelSel || hoversettings || hovermusicRoom ||	hoverQuit) || hoverAddress != hoverAddressOld) {
		rsc->sptr->playSFX(1);
		playedOnce = true;
	}
	else if(playedOnce && (hoverLevelSel || hoversettings || hovermusicRoom || hoverQuit)){
		playedOnce = true;
	}
	else {
		playedOnce = false;
	}

	if (hoverLevelSel) {
		hoverAddressOld = &hoverLevelSel;
	}
	else if (hoversettings) {
		hoverAddressOld = &hoversettings;
	}
	else if (hovermusicRoom) {
		hoverAddressOld = &hovermusicRoom;
	}
	else if (hoverQuit) {
		hoverAddressOld = &hoverQuit;
	}
}


void Menu::render()
{
	SDL_RenderClear(renderer);

	SDL_RenderCopy(renderer, rsc->gameBG.at(T1BG), NULL, NULL);

	//	SDL_Rect a = { 200,0,1000,1000 };
	//SDL_RenderCopy(renderer, rsc->gameAnimations.at(1).at(2), NULL, &a);

	if (hoverLevelSel) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(60), NULL, &rectLevelSelect);
	}
	else if (hoversettings) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(60), NULL, &rectSettings);
	}
	else if (hovermusicRoom) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(60), NULL, &rectMusicRoom);
	}
	else if (hoverQuit) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(60), NULL, &rectQuit);
	}

	SDL_RenderCopy(renderer, textLevelSelect, NULL, &rectLevelSelect);
	SDL_RenderCopy(renderer, textSettings, NULL, &rectSettings);
	SDL_RenderCopy(renderer, textMusicRoom, NULL, &rectMusicRoom);
	SDL_RenderCopy(renderer, textQuit, NULL, &rectQuit);


	SDL_RenderPresent(renderer);
}


