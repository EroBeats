#include "Language.h"

#include "RescourceKeys.h"
#include "ScreenNames.h"

Language::Language(int* scrn, FileIO* fileIO, ResourceMaster* resourcePointer)
{

	screen = scrn;
	fio = fileIO;
	rsc = resourcePointer;
	renderer = rsc->rendPtr;

	x;
	y;

	font = new Fonts(0, rsc);
	outlineAnimation = new Animation(rsc->gameAnimations.at(WhiteBlueBG));

	setting = fio->loadUserData(3);

	setTextures();
	//std::cout << FPSDelay << "FPS\n";
	gameLoop();
}

Language::~Language() {

}

void Language::setTextures() {
	int row1 = font->prcnt(0.245, 'y');
	int row2 = font->prcnt(0.5, 'y');

	int flagWidth = font->prcnt(0.315, 'x');
	int flagHeight = font->prcnt(0.20, 'y');

	int wider = font->prcnt(0.02, 'y');

	rectFlagEN = { font->prcnt(0.015, 'x'), row1 ,flagWidth, flagHeight };
	largeRectFlagEN = {rectFlagEN.x - font->prcnt(0.006, 'x'), row1 - font->prcnt(0.008, 'y'),flagWidth + wider, flagHeight + wider };

	rectFlagFR = { font->prcnt(0.34, 'x'), row1 ,flagWidth, flagHeight };
	largeRectFlagFR = { rectFlagFR.x - font->prcnt(0.006, 'x'), row1 - font->prcnt(0.008, 'y') ,flagWidth + wider, flagHeight + wider };

	rectFlagJP = { font->prcnt(0.665, 'x'), row1 ,flagWidth,flagHeight };
	largeRectFlagJP = { rectFlagJP.x - font->prcnt(0.006, 'x'), row1 - font->prcnt(0.008, 'y') ,flagWidth + wider, flagHeight + wider };

	font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(0), 3);
	textEN = font->getTexture();
	rectEN = { font->prcnt(0.10, 'x'), row2 ,font->prcnt(0.10, 'x') ,font->prcnt(0.10, 'y') };

	font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(1), 3);
	textFR = font->getTexture();
	rectFR = { font->prcnt(0.44, 'x'), row2 ,font->prcnt(0.10, 'x') ,font->prcnt(0.10, 'y') };

	font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(2), 3);
	textJP = font->getTexture();
	rectJP = { font->prcnt(0.765, 'x'), row2 ,font->prcnt(0.10, 'x') ,font->prcnt(0.10, 'y') };

}

void Language::destroyFonts() {
	SDL_DestroyTexture(textEN);
	SDL_DestroyTexture(textFR);
	SDL_DestroyTexture(textJP);
}


void Language::update()
{
	hoverEN = false;
	hoverFR = false;
	hoverJP = false;

	if (x > rectFlagEN.x && x < (rectFlagEN.x + rectFlagEN.w) && y > rectFlagEN.y && y < (rectFlagEN.y + rectFlagEN.h)) {
		hoverEN = true;
	}
	else if (x > rectFlagFR.x && x < (rectFlagFR.x + rectFlagFR.w) && y > rectFlagFR.y && y < (rectFlagFR.y + rectFlagFR.h)) {
		hoverFR = true;
	}
	else if (x > rectFlagJP.x && x < (rectFlagJP.x + rectFlagJP.w) && y > rectFlagJP.y && y < (rectFlagJP.y + rectFlagJP.h)) {
		hoverJP = true;
	}

	if (hoverEN && !playedOnce) {
		fio->loadLanguage("Language/EN.txt");
		setting = setting / 10 * 10;
		setting += 1;
		destroyFonts();
		setTextures();
	}
	else if (hoverFR && !playedOnce) {
		fio->loadLanguage("Language/FR.txt");
		setting = setting / 10 * 10;
		setting += 2;
		destroyFonts();
		setTextures();
	}
	else if(hoverJP && !playedOnce){
		fio->loadLanguage("Language/JP.txt");
		setting = setting / 10 * 10;
		setting += 3;
		destroyFonts();
		setTextures();
	}
	
	if (mouseClicked && (hoverEN ||	hoverFR ||	hoverJP )) {
		*screen = MainMenuScreen;
		fio->saveUserData(setting, 3);
		running = false;
	}
}

void Language::play() 
{
	if (hoverEN) {
		hoverAddress = &hoverEN;
	}
	else if (hoverFR) {
		hoverAddress = &hoverFR;
	}
	else if (hoverJP) {
		hoverAddress = &hoverJP;
	}

	if (mouseClicked && (hoverJP || hoverFR || hoverEN)) {
		rsc->sptr->playSFX(0);
	}

	if (!playedOnce && (hoverJP || hoverFR || hoverEN || hoverAddress != hoverAddressOld)) {
		rsc->sptr->playSFX(1);
		playedOnce = true;
		//std::cout << "1S\n";
	}
	else if (playedOnce && (hoverJP || hoverFR || hoverEN) && hoverAddress == hoverAddressOld) {
		playedOnce = true;
		//std::cout << "2S\n";
	}
	else {
		playedOnce = false;
	}

	if (hoverEN) {
		hoverAddressOld = &hoverEN;
	}
	else if (hoverFR) {
		hoverAddressOld = &hoverFR;
	}
	else if (hoverJP) {
		hoverAddressOld = &hoverJP;
	}
}

void Language::render()
{
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, rsc->gameBG.at(LangBG), NULL, NULL);
	if (hoverEN) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &largeRectFlagEN);
	}
	else if (hoverFR) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &largeRectFlagFR);
	}
	else if (hoverJP) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &largeRectFlagJP);
	}
	SDL_RenderCopy(renderer, rsc->gameGraphics.at(EnFlag), NULL, &rectFlagEN);

	SDL_RenderCopy(renderer, rsc->gameGraphics.at(FrFlag), NULL, &rectFlagFR);

	SDL_RenderCopy(renderer, rsc->gameGraphics.at(JpFlag), NULL, &rectFlagJP);

	SDL_RenderCopy(renderer, textEN, NULL, &rectEN);

	SDL_RenderCopy(renderer, textFR, NULL, &rectFR);

	SDL_RenderCopy(renderer, textJP, NULL, &rectJP);

	SDL_RenderPresent(renderer);

}

void Language::close()
{
	delete font;
	delete outlineAnimation;
	destroyFonts();
}
