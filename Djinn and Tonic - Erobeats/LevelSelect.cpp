#include "LevelSelect.h"

#include "ScreenNames.h"
#include "RescourceKeys.h"

//Display boxes with clear ratings { F, C-, B, A+}
LevelSelect::LevelSelect(){}

LevelSelect::LevelSelect(int* screenPointer, FileIO* filePntr, ResourceMaster* resourcePntr)
{
	screen = screenPointer;
	rsc = resourcePntr;
	renderer = rsc->rendPtr;
	fio = filePntr;

	outlineAnimation = new Animation(rsc->gameAnimations.at(0));

	numUserData = fio->loadUserData(LevelUnlocks);
	numUserScores = fio->loadUserData(LevelScores);

	boxSize = { 100, 100, 100, 100 };

	font = new Fonts(0, resourcePntr);

	int textSizeX = font->prcnt(0.20, 'x');
	int textSizeY = font->prcnt(0.15, 'y');
	int textAlignX = font->prcnt(0.55, 'x');
	int textOffsetY = font->prcnt(0.05, 'y');

	int generalSpaceY = font->prcnt(0.15, 'y');

	int boxSizeX = font->prcnt(0.10, 'x');
	int boxSizeY = font->prcnt(0.10, 'y');
	int boxAlignX = font->prcnt(0.85, 'x');
	int boxOffsetY = font->prcnt(0.08, 'y');
	
	if ((numUserData % 10) / 1 > 0) {
		level1Selectable = true;
		font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(Level1Text), 2);
		rectLevel1Box = { boxAlignX, generalSpaceY * 0 + boxOffsetY,boxSizeX ,boxSizeY};

		int score = (numUserScores % 10) / 1;
		if (score == 1) {
			textLevel1Box = rsc->gameGraphics.at(GradeF);
		}
		else if (score == 2) {
			textLevel1Box = rsc->gameGraphics.at(GradeC);
		}
		else if (score == 3) {
			textLevel1Box = rsc->gameGraphics.at(GradeB);
		}
		else if (score == 4) {
			textLevel1Box = rsc->gameGraphics.at(GradeA);
		}
	}
	else {
		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Level1Text), 2);
	}
	textLevel1 = font->getTexture();
	rectLevel1 = { textAlignX ,generalSpaceY * 0 + textOffsetY,textSizeX,textSizeY };

	if ((numUserData % 100) / 10 > 0) {
		level2Selectable = true;
		font->loadFont(SDL_Color{  200,200,200,00  }, rsc->gameText.at(Level2Text), 2);
		rectLevel2Box = { boxAlignX,generalSpaceY * 1 + boxOffsetY,boxSizeX ,boxSizeY };

		int score = (numUserScores % 100) / 10;
		if (score == 1) {
			textLevel2Box = rsc->gameGraphics.at(GradeF);
		}
		else if (score == 2) {
			textLevel2Box = rsc->gameGraphics.at(GradeC);
		}
		else if (score == 3) {
			textLevel2Box = rsc->gameGraphics.at(GradeB);
		}
		else if (score == 4) {
			textLevel2Box = rsc->gameGraphics.at(GradeA);
		}
	}
	else {
		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Level2Text), 2);
	}
	textLevel2 = font->getTexture();
	rectLevel2 = { textAlignX,generalSpaceY * 1 + textOffsetY,textSizeX,textSizeY };

	if ((numUserData % 1000) / 100 > 0) {
		level3Selectable = true;
		font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(Level3Text), 2);
		rectLevel3Box = { boxAlignX,generalSpaceY * 2 + boxOffsetY,boxSizeX ,boxSizeY };

		int score = (numUserScores % 1000) / 100;
		if (score == 1) {
			textLevel3Box = rsc->gameGraphics.at(GradeF);
		}
		else if (score == 2) {
			textLevel3Box = rsc->gameGraphics.at(GradeC);
		}
		else if (score == 3) {
			textLevel3Box = rsc->gameGraphics.at(GradeB);
		}
		else if (score == 4) {
			textLevel3Box = rsc->gameGraphics.at(GradeA);
		}
	}
	else {
		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Level3Text), 2);
	}

	textLevel3 = font->getTexture();
	rectLevel3 = { textAlignX ,generalSpaceY * 2 + textOffsetY,textSizeX,textSizeY };

	if ((numUserData % 10000) / 1000 > 0) {
		level4Selectable = true;
		font->loadFont(SDL_Color{  200,200,200,00  }, rsc->gameText.at(Level4Text), 2);
		//rectLevel4Box = { boxAlignX,generalSpaceY * 3 + boxOffsetY,boxSizeX ,boxSizeY };

		int score = (numUserScores % 10000) / 1000;
		if (score == 1) {
			//textLevel4Box = rsc->gameGraphics.at(GradeF);
		}
		else if (score == 2) {
			//textLevel4Box = rsc->gameGraphics.at(GradeC);
		}
		else if (score == 3) {
			//textLevel4Box = rsc->gameGraphics.at(GradeB);
		}
		else if (score == 4) {
			//textLevel4Box = rsc->gameGraphics.at(GradeA);
		}
	}
	else {
		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Level4Text), 2);
	}

	//textLevel4 = font->getTexture();
	//rectLevel4 = {textAlignX,generalSpaceY * 3 + textOffsetY,textSizeX,textSizeY };


	if ((numUserData % 100000) / 10000 > 0) {
		remixSelectable = true;
		font->loadFont(SDL_Color{  200,200,200,00  }, rsc->gameText.at(RemixText), 2);
		//rectRemixBox = { boxAlignX,generalSpaceY * 4 + boxOffsetY,boxSizeX ,boxSizeY };

		int score = (numUserScores % 100000) / 10000;
		if (score == 1) {
			//textRemixBox = rsc->gameGraphics.at(GradeF);
		}
		else if (score == 2) {
			//textRemixBox = rsc->gameGraphics.at(GradeC);
		}
		else if (score == 3) {
			//textRemixBox = rsc->gameGraphics.at(GradeB);
		}
		else if (score == 4) {
			//textRemixBox = rsc->gameGraphics.at(GradeA);
		}
	}
	else {
		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(RemixText), 2);
	}
	//textRemix = font->getTexture();
	//rectRemix = {textAlignX, generalSpaceY * 4 + textOffsetY,textSizeX,textSizeY };
//
	font->loadFont(SDL_Color{  200,200,200,00  }, rsc->gameText.at(BackText), 2);
	textQuit = font->getTexture();
	rectQuit = { textAlignX,generalSpaceY * 3 + textOffsetY,textSizeX,textSizeY };

	gameLoop();
}


LevelSelect::~LevelSelect()
{
}

void LevelSelect::update()
{
	hoverLevel1 = false;
	hoverLevel2 = false;
	hoverLevel3 = false;
	hoverLevel4 = false;
	hoverRemix = false;
	hoverQuit = false;

	if (x > rectQuit.x && y > rectQuit.y) {
		//cout << "ovr6\n";
		hoverQuit = true;
	}
//	else if (x > rectRemix.x && y >  rectRemix.y && remixSelectable) {
		//cout << "ovr5\n";
//		hoverRemix = true;
//	}
//	else if (x > rectLevel4.x && y > rectLevel4.y && level4Selectable) {
		//cout << "ovr4\n";
//		hoverLevel4 = true;
//	}
	else if (x > rectLevel3.x  && y > rectLevel3.y && level3Selectable) {
		//cout << "ovr3\n";
		hoverLevel3 = true;
	}
	else if (x > rectLevel2.x  && y > rectLevel2.y && level2Selectable) {
		//cout << "ovr2\n";
		hoverLevel2 = true;
	}
	else if (x > rectLevel1.x  && y > rectLevel1.y && level1Selectable) {
		//cout << "ovr1\n";
		hoverLevel1 = true;
	}
	else {
		hoverLevel1 = false;
		hoverLevel2 = false;
		hoverLevel3 = false;
		hoverLevel4 = false;
		hoverRemix= false;
		hoverQuit = false;
	}

	if (mouseClicked && hoverLevel1) {
		//cout << "Fired1\n";
		running = false;
		*screen = Level1Screen;
	}
	else if (mouseClicked && hoverLevel2) {
		//cout << "Fired2\n";
		running = false;
		*screen = SettingsScreen;
	}
	else if (mouseClicked && hoverLevel3) {
		//cout << "Fired3\n";
		running = false;
		*screen = Level3Screen;
	}
	else if (mouseClicked && hoverLevel4) {
		//cout << "Fired4\n";
		running = false;
		*screen = SettingsScreen;
	}
	else if (mouseClicked && hoverRemix) {
		//cout << "Fired5\n";
		running = false;
		*screen = SettingsScreen;
	}
	else if (mouseClicked && hoverQuit) {
		//cout << "Fired6\n";
		running = false;
		*screen = MainMenuScreen;
	}

	if (mouseClickedR) {
		running = false;
		*screen = MainMenuScreen;
	}
}

void LevelSelect::play()
{
	if (hoverLevel1) {
		hoverAddress = &hoverLevel1;
	}
	else if (hoverLevel2) {
		hoverAddress = &hoverLevel2;
	}
	else if (hoverLevel3) {
		hoverAddress = &hoverLevel3;
	}
	else if (hoverLevel4) {
		hoverAddress = &hoverLevel4;
	}
	else if (hoverRemix) {
		hoverAddress = &hoverRemix;
	}
	else if (hoverQuit) {
		hoverAddress = &hoverQuit;
	}

	if (mouseClicked && (hoverLevel1 || hoverLevel2 || hoverLevel3 || hoverLevel4 || hoverRemix || hoverQuit)) {
		rsc->sptr->playSFX(0);
	}
	if (!playedOnce && (hoverLevel1 || hoverLevel2 || hoverLevel3 || hoverLevel4 || hoverRemix || hoverQuit) || hoverAddress != hoverAddressOld) {
		rsc->sptr->playSFX(1);
		playedOnce = true;
	}
	else if (playedOnce && (hoverLevel1 || hoverLevel2 || hoverLevel3 || hoverLevel4 || hoverRemix || hoverQuit)) {
		playedOnce = true;
	}
	else {
		playedOnce = false;
	}

	if (hoverLevel1) {
		hoverAddressOld = &hoverLevel1;
	}
	else if (hoverLevel2) {
		hoverAddressOld = &hoverLevel2;
	}
	else if (hoverLevel3) {
		hoverAddressOld = &hoverLevel3;
	}
	else if (hoverLevel4) {
		hoverAddressOld = &hoverLevel4;
	}
	else if (hoverRemix) {
		hoverAddressOld = &hoverRemix;
	}
	else if (hoverQuit) {
		hoverAddressOld = &hoverQuit;
	}
}


void LevelSelect::render()
{
	SDL_RenderClear(renderer);

	SDL_RenderCopy(renderer, rsc->gameBG.at(T2BG), NULL, NULL);

	if (hoverLevel1) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectLevel1);
	}
	else if (hoverLevel2) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectLevel2);
	}
	else if (hoverLevel3) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectLevel3);
	}
	else if (hoverLevel4) {
//		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectLevel4);
	}
	else if (hoverRemix) {
//		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectRemix);
	}
	else if (hoverQuit) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectQuit);
	}

	SDL_SetRenderDrawColor(renderer, 0xff, 0xff, 0xff, 0xff);
	SDL_RenderCopy(renderer, textLevel1, NULL, &rectLevel1);
	if (level1Selectable) {
		SDL_RenderDrawRect(renderer, &rectLevel1Box);
		SDL_RenderCopy(renderer, textLevel1Box, NULL, &rectLevel1Box);
	}
	SDL_RenderCopy(renderer, textLevel2, NULL, &rectLevel2);
	if (level1Selectable) {
		SDL_RenderDrawRect(renderer, &rectLevel2Box);
		SDL_RenderCopy(renderer, textLevel2Box, NULL, &rectLevel2Box);
	}
	SDL_RenderCopy(renderer, textLevel3, NULL, &rectLevel3);
	if (level1Selectable) {
		SDL_RenderDrawRect(renderer, &rectLevel3Box);
		SDL_RenderCopy(renderer, textLevel3Box, NULL, &rectLevel3Box);
	}
	//SDL_RenderCopy(renderer, textLevel4, NULL, &rectLevel4);
	if (level1Selectable) {
	//	SDL_RenderDrawRect(renderer, &rectLevel4Box);
	//	SDL_RenderCopy(renderer, textLevel4Box, NULL, &rectLevel4Box);
	}
	//SDL_RenderCopy(renderer, textRemix, NULL, &rectRemix);
	if (remixSelectable) {
	//	SDL_RenderDrawRect(renderer, &rectRemixBox);
	//	SDL_RenderCopy(renderer, textRemixBox, NULL, &rectRemixBox);
	}
	SDL_RenderCopy(renderer, textQuit, NULL, &rectQuit);

	SDL_SetRenderDrawColor(renderer, 0x22, 0x22, 0x22, 0x22);
	SDL_RenderPresent(renderer);
}

void LevelSelect::close()
{
	SDL_DestroyTexture(textLevel1);
	SDL_DestroyTexture (textLevel2);
	SDL_DestroyTexture (textLevel3);
	//SDL_DestroyTexture (textLevel4);
	//SDL_DestroyTexture(textRemix);
	SDL_DestroyTexture(textQuit);
}
