 #pragma once

#include "ScreenTemplate.h"

#include "RhythmIO.h"
#include "TempoTools.h"

#include <chrono>
class LevelTemplate : public ScreenTemplate
{
public:
	LevelTemplate();
	~LevelTemplate();

	void processInput();

	void initializeBeats(std::string levelPath);
	void startMusic(int track, int fadeInDuration);

	double scoreBarPercent(int currScore, int maxScore);

	RhythmIO* rio;
	TempoTools* tTools;
	
	short playSound[9];
	short playCue[9];

	bool heldL;
	bool heldR;
	bool leftMouseClicked;
	bool rightMouseClicked;
	bool playedOnce1;
	bool playedOnce2;
	bool playedOnce2_1;
	bool playedOnce3;
	bool playedOnce4;

};


