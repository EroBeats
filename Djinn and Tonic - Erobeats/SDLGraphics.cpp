#include "SDLGraphics.h"
#include "SettingsInit.h"
#include "RescourceKeys.h"

//some of this should be moved to fileio

SDLGraphics::SDLGraphics(FileIO* filePntr, ResourceMaster* rscPntr)
{

	rsc = rscPntr;
	fio = filePntr;

	loaded = false;

	SDL_Init(SDL_INIT_EVERYTHING);
	createWindow();

	SDL_SetWindowTitle(window, "EroRhythm 1: Entranced");


}


SDLGraphics::~SDLGraphics()
{
}

void SDLGraphics::createWindow() {
	window = SDL_CreateWindow("Djinn and Tonic", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1000, 800, SDL_WINDOW_HIDDEN | SDL_WINDOW_OPENGL);
	//window = SDL_CreateWindow("Djinn and Tonic", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, SDL_WINDOW_FULLSCREEN);

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(renderer, 0x22, 0x22, 0x22, 0x22);

}

void SDLGraphics::createLoadScreen() {

	createPointers();

	fio->loadAnimationsZipInterval(0, 10);
	fio->loadTexturesZip();

	fio->loadBGZip();

	SDL_ShowWindow(window);
	SDL_SetWindowBordered(window, SDL_FALSE);
	SplashScreen loading(&loaded, rsc);

	//fio->loadAnimationsZip();
	//fio->loadAnimationsZipInterval(16, 30);

	SDL_SetWindowBordered(window, SDL_TRUE);
	setupGame();
}

void SDLGraphics::startThread() {
	std::thread loadThread(&SDLGraphics::setupGraphics, this, fio);
	loadThread.detach();
}

void SDLGraphics::createPointers() {
	rsc->rendPtr = renderer;
	rsc->window = window;
}

void SDLGraphics::loadResources(FileIO* fio) {
	fio->loadBGMZip();
	fio->loadSFXZip();
}
void SDLGraphics::setupGame() {
	SettingsInit pi(fio, rsc);

	switch (pi.getResolution()) {
	case 1:
		screenWidth = 600;
		screenHeight = 480;
		break;
	case 2:
		screenWidth = 1000;
		screenHeight = 800;
		SDL_SetWindowPosition(window, 200, 200);
		break;
	case 3:
		screenWidth = 1250;
		screenHeight = 1000;
		SDL_SetWindowPosition(window, SDL_WINDOWPOS_CENTERED, 25);
		break;
	default:
		screenWidth = 1000;
		screenHeight = 800;
		SDL_SetWindowPosition(window, 200, 200);
		break;
	}
	SDL_SetWindowSize(window, screenWidth, screenHeight);


}
