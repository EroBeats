#include "MusicRoom.h"

//modify into the play room
//Need to run game sprites.
//Probably will be the last thing done.

#include "ScreenNames.h"
#include "RescourceKeys.h"

MusicRoom::MusicRoom()
{
}

MusicRoom::MusicRoom(int* screenPointer, FileIO* filePntr, ResourceMaster* resourcePntr)
{



	screen = screenPointer;
	rsc = resourcePntr;
	renderer = rsc->rendPtr;
	fio = filePntr;

	FPSDelay = 16;

	outlineAnimation = new Animation(rsc->gameAnimations.at(WhiteBlueBG));

	L2A1 = new Animation(rsc->gameAnimations.at(L3Animation1OK));

	std::vector<SDL_Texture*> L1pump3R_R = fio->loadSpecificZip("1_3.zip");
	std::vector<SDL_Texture*> L2pump3R_R = fio->loadSpecificZip("2_3.zip");
	std::vector<SDL_Texture*> L3pump3R_R = fio->loadSpecificZip("3_3.zip");

	std::vector<SDL_Texture*> L2A2combo;

	L2A2combo.reserve(rsc->gameAnimations.at(L3Animation2_1).size() + rsc->gameAnimations.at(L3Animation2_2).size() +
		rsc->gameAnimations.at(L3Animation2_3).size() + L2pump3R_R.size() + rsc->gameAnimations.at(L3Animation3_1).size() + rsc->gameAnimations.at(L3Animation3_2).size() +
		rsc->gameAnimations.at(L3Animation3_3).size() +	rsc->gameAnimations.at(L3Animation3_4).size());

	L2A2combo.insert(L2A2combo.end(), rsc->gameAnimations.at(L3Animation2_1).begin(), rsc->gameAnimations.at(L3Animation2_1).end());
	L2A2combo.insert(L2A2combo.end(), rsc->gameAnimations.at(L3Animation2_2).begin(), rsc->gameAnimations.at(L3Animation2_2).end());
	L2A2combo.insert(L2A2combo.end(), rsc->gameAnimations.at(L3Animation2_3).begin(), rsc->gameAnimations.at(L3Animation2_3).end());
	L2A2combo.insert(L2A2combo.end(), L2pump3R_R.begin(), L2pump3R_R.end());
	L2A2combo.insert(L2A2combo.end(), rsc->gameAnimations.at(L3Animation3_1).begin(), rsc->gameAnimations.at(L3Animation3_1).end());
	L2A2combo.insert(L2A2combo.end(), rsc->gameAnimations.at(L3Animation3_2).begin(), rsc->gameAnimations.at(L3Animation3_2).end());
	L2A2combo.insert(L2A2combo.end(), rsc->gameAnimations.at(L3Animation3_3).begin(), rsc->gameAnimations.at(L3Animation3_3).end());
	L2A2combo.insert(L2A2combo.end(), rsc->gameAnimations.at(L3Animation3_4).begin(), rsc->gameAnimations.at(L3Animation3_4).end());

	L2A2 = new Animation(L2A2combo);

	numUserData = fio->loadUserData(LevelUnlocks);
	numUserScores = fio->loadUserData(LevelScores);

	font = new Fonts(0, resourcePntr);

	generateTextLeft();

	rectMainBox = { font->prcnt(0.2, 'x'),font->prcnt(0.0, 'y'), font->prcnt(1.0, 'x'), font->prcnt(1.0, 'y') };
	rectSelectionBox = { font->prcnt(0.0, 'x'),font->prcnt(0.0, 'y'), font->prcnt(0.2, 'x'), font->prcnt(0.65, 'y') };

	gameLoop();
}


MusicRoom::~MusicRoom()
{
}

void MusicRoom::destroyTextLeft() {
	SDL_DestroyTexture(textLevel1);
	SDL_DestroyTexture(textLevel2);
	SDL_DestroyTexture(textLevel3);
	SDL_DestroyTexture(textQuit);
}

void MusicRoom::generateTextLeft() {
	destroyTextLeft();

	int textSizeX = font->prcnt(0.09, 'x');
	int textSizeY = font->prcnt(0.1, 'y');
	int largetextSizeX = font->prcnt(0.20, 'x');
	int lagretextSizeY = font->prcnt(0.15, 'y');

	int rowOne = font->prcnt(0.005, 'x');
	int rowTwo = font->prcnt(0.105, 'x');

	int colOne = font->prcnt(0.05, 'y');
	int colTwo = font->prcnt(0.20, 'y');
	int colThree = font->prcnt(0.35, 'y');
	int colFour = font->prcnt(0.50, 'y');

	int textAlignX = font->prcnt(0.10, 'x');

	

	if ((numUserData % 10) / 1 > 0) {
		level1Selectable = true;
		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Level1Text), 2);
	}
	else {
		font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(Level1Text), 2);
	}
	textLevel1 = font->getTexture();
	rectLevel1 = { rowOne , colOne, textSizeX, textSizeY };

	if ((numUserData % 100) / 10 > 0) {
		level2Selectable = true;
		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Level2Text), 2);
	}
	else {
		font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(Level2Text), 2);
	}
	textLevel2 = font->getTexture();
	rectLevel2 = { rowOne , colTwo, textSizeX, textSizeY };

	if ((numUserData % 1000) / 100 > 0) {
		level3Selectable = true;
		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Level3Text), 2);
	}
	else {
		font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(Level3Text), 2);
	}
	textLevel3 = font->getTexture();
	rectLevel3 = { rowOne, colThree, textSizeX, textSizeY };

	font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(BackText), 2);
	textQuit = font->getTexture();
	rectQuit = { rowOne , colFour, textSizeX, textSizeY };
}

void MusicRoom::destroyTextRight() {
	SDL_DestroyTexture(textBGM);
	SDL_DestroyTexture(textA1);
	SDL_DestroyTexture(textA2);
	SDL_DestroyTexture(textEC);
}

void MusicRoom::generateTextRight(int level) {

	destroyTextRight();

	int textSizeX = font->prcnt(0.09, 'x');
	int textSizeY = font->prcnt(0.1, 'y');
	int largetextSizeX = font->prcnt(0.20, 'x');
	int lagretextSizeY = font->prcnt(0.15, 'y');

	int rowOne = font->prcnt(0.005, 'x');
	int rowTwo = font->prcnt(0.105, 'x');

	int colOne = font->prcnt(0.05, 'y');
	int colTwo = font->prcnt(0.20, 'y');
	int colThree = font->prcnt(0.35, 'y');
	int colFour = font->prcnt(0.50, 'y');

	bool grayTrue = false;

	switch (level) {
	case 1:
		if (numUserScores % 10 / 1 > 1) grayTrue = true;
		break;
	case 2:
		if (numUserScores % 100 / 10 > 1) grayTrue = true; break;
	case 3:
		if (numUserScores % 1000 / 100 > 1) grayTrue = true; break;
	}

	if (grayTrue) {
		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Song1Text), 2);
		textBGM = font->getTexture();
		rectBGM = { rowTwo , colOne, textSizeX, textSizeY };

		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Animation1Text), 2);
		textA1 = font->getTexture();
		rectA1 = { rowTwo , colTwo, textSizeX, textSizeY };

		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(Animation2Text), 2);
		textA2 = font->getTexture();
		rectA2 = { rowTwo , colThree, textSizeX, textSizeY };

		font->loadFont(SDL_Color{ 100,100,100,128 }, rsc->gameText.at(EndCardText), 2);
		textEC = font->getTexture();
		rectEC = { rowTwo , colFour, textSizeX, textSizeY };
		BGMSelectable = true;
		A1Selectable = true;
		A2Selectable = true;
		ECSelectable = true;
	}
	else {
		font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(Song1Text), 2);
		textBGM = font->getTexture();
		rectBGM = { rowTwo , colOne, textSizeX, textSizeY };

		font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(Animation1Text), 2);
		textA1 = font->getTexture();
		rectA1 = { rowTwo , colTwo, textSizeX, textSizeY };

		font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(Animation2Text), 2);
		textA2 = font->getTexture();
		rectA2 = { rowTwo , colThree, textSizeX, textSizeY };

		font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(EndCardText), 2);
		textEC = font->getTexture();
		rectEC = { rowTwo , colFour, textSizeX, textSizeY };

		BGMSelectable = false;
		A1Selectable = false;
		A2Selectable = false;
		ECSelectable = false;
	}
}

void MusicRoom::update()
{
	hoverLevel1 = false;
	hoverLevel2 = false;
	hoverLevel3 = false;
	hoverQuit = false;
	hoverBGM = false;
	hoverA1 = false;
	hoverA2 = false;
	hoverEC = false;

	if (x > rectQuit.x && x < (rectQuit.x + rectQuit.w) && y > rectQuit.y && y < rectQuit.y + rectQuit.h) {
		//cout << "ovr6\n";
		hoverQuit = true;
	}
	else if (x > rectLevel3.x  && x < (rectLevel3.x + rectLevel3.w) && y > rectLevel3.y && y < rectLevel3.y + rectLevel3.h && level3Selectable) {
		//cout << "ovr3\n";
		hoverLevel3 = true;
	}
	else if (x > rectLevel2.x  && x < (rectLevel2.x + rectLevel2.w) && y > rectLevel2.y && y < rectLevel2.y + rectLevel2.h && level2Selectable) {
		//cout << "ovr2\n";
		hoverLevel2 = true;
	}
	else if (x > rectLevel1.x  && x < (rectLevel1.x + rectLevel1.w) && y > rectLevel1.y && y < rectLevel1.y + rectLevel1.h && level1Selectable) {
		//cout << "ovr1\n";
		hoverLevel1 = true;
	}
	else if (x < rectBGM.x + rectBGM.w && x > rectBGM.x && y < (rectBGM.y + rectBGM.h) && BGMSelectable) {
		//cout << "ovr3\n";
		hoverBGM = true;
	}
	else if (x < rectA1.x + rectA1.w && x > rectA1.x  && y < (rectA1.y + rectA1.h) && A1Selectable) {
		//cout << "ovr2\n";
		hoverA1 = true;
	}
	else if (x < rectA2.x + rectA2.w && x > rectA2.x  && y < (rectA2.y + rectA2.h) && A2Selectable) {
		//cout << "ovr1\n";
		hoverA2 = true;
	}
	else if (x < rectEC.x + rectEC.w && x > rectEC.x && y < (rectEC.y + rectEC.h) && ECSelectable) {
		//cout << "ovr1\n";
		hoverEC = true;
	}

	if (mouseClicked && hoverLevel1) {
		//cout << "Fired1\n";
		generateTextRight(1);
		levelNumber = 1;
	}
	else if (mouseClicked && hoverLevel2) {
		//cout << "Fired2\n";
		generateTextRight(2);
		levelNumber = 2;
	}
	else if (mouseClicked && hoverLevel3) {
		//cout << "Fired3\n";
		generateTextRight(3);
		levelNumber = 3;
	}
	else if (mouseClicked && hoverQuit) {
		//cout << "Fired6\n";
		running = false;
		*screen = MainMenuScreen;
	}
	else if (mouseClicked && hoverA1) {
		playA1 = true;
		playA2 = false;
		displayEC = false;
	}
	else if (mouseClicked && hoverA2) {
		playA1 = false;
		playA2 = true;
		displayEC = false;
	}
	else if (mouseClicked && hoverEC) {
		playA1 = false;
		playA2 = false;
		displayEC = true;
	}
}

void MusicRoom::play()
{
	if (hoverLevel1) {
		hoverAddress = &hoverLevel1;
	}
	else if (hoverLevel2) {
		hoverAddress = &hoverLevel2;
	}
	else if (hoverLevel3) {
		hoverAddress = &hoverLevel3;
	}
	else if (hoverQuit) {
		hoverAddress = &hoverQuit;
	}
	else if (hoverBGM) {
		hoverAddress = &hoverBGM;
	}
	else if (hoverA1) {
		hoverAddress = &hoverA1;
	}
	else if (hoverA2) {
		hoverAddress = &hoverA2;
	}
	else if (hoverEC) {
		hoverAddress = &hoverEC;
	}

	if (mouseClicked && (hoverLevel1 || hoverLevel2 || hoverLevel3 || hoverQuit || hoverEC || hoverA2 || hoverA1 || hoverBGM) && !playedOnceL) {
		rsc->sptr->playSFX(0);
		if (hoverBGM) {
			rsc->bptr->playBGM(levelNumber);
		}
		playedOnceL = true;
	}
	else if (mouseClicked && playedOnceL) {
		playedOnceL = true;
	}
	else {
		playedOnceL = false;
	}

	if (!playedOnce && (hoverLevel1 || hoverLevel2 || hoverLevel3 || hoverQuit || hoverEC || hoverA2 || hoverA1 || hoverBGM) || hoverAddress != hoverAddressOld) {
		rsc->sptr->playSFX(1);
		playedOnce = true;
	}
	else if (playedOnce && (hoverLevel1 || hoverLevel2 || hoverLevel3 || hoverQuit || hoverEC || hoverA2 || hoverA1 || hoverBGM)) {
		playedOnce = true;
	}
	else {
		playedOnce = false;
	}

	if (hoverLevel1) {
		hoverAddressOld = &hoverLevel1;
	}
	else if (hoverLevel2) {
		hoverAddressOld = &hoverLevel2;
	}
	else if (hoverLevel3) {
		hoverAddressOld = &hoverLevel3;
	}
	else if (hoverQuit) {
		hoverAddressOld = &hoverQuit;
	}
	else if (hoverBGM) {
		hoverAddressOld  = &hoverBGM;
	}
	else if (hoverA1) {
		hoverAddressOld  = &hoverA1;
	}
	else if (hoverA2) {
		hoverAddressOld  = &hoverA2;
	}
	else if (hoverEC) {
		hoverAddressOld  = &hoverEC;
	}
}


void MusicRoom::render()
{
	SDL_RenderClear(renderer);

	SDL_RenderCopy(renderer, rsc->gameBG.at(T3BG), NULL, NULL);

	SDL_SetRenderDrawColor(renderer, 0xaf, 0xaf, 0xaf, 0x00);
	SDL_RenderFillRect(renderer, &rectSelectionBox);
	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);

	if (hoverLevel1) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectLevel1);
	}
	else if (hoverLevel2) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectLevel2);
	}
	else if (hoverLevel3) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectLevel3);
	}
	else if (hoverQuit) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectQuit);
	}
	else if (hoverBGM) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectBGM);
	}
	else if (hoverA1) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectA1);
	}
	else if (hoverA2) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectA2);
	}
	else if (hoverEC) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectEC);
	}

	SDL_RenderCopy(renderer, textLevel1, NULL, &rectLevel1);
	SDL_RenderCopy(renderer, textLevel2, NULL, &rectLevel2);
	SDL_RenderCopy(renderer, textLevel3, NULL, &rectLevel3);
	SDL_RenderCopy(renderer, textQuit, NULL, &rectQuit);

	SDL_RenderCopy(renderer, textBGM, NULL, &rectBGM);
	SDL_RenderCopy(renderer, textA1, NULL, &rectA1);
	SDL_RenderCopy(renderer, textA2, NULL, &rectA2);
	SDL_RenderCopy(renderer, textEC, NULL, &rectEC);

	SDL_RenderFillRect(renderer, &rectMainBox);
	if (playA1) {
		switch (levelNumber) {
			case 1: 			SDL_RenderCopy(renderer, rsc->gameBG.at(Level3BG), NULL, &rectMainBox);
								SDL_RenderCopy(renderer, L2A1->itterateAnimation(30), NULL, &rectMainBox);break;
		case 2: 
			SDL_RenderCopy(renderer, rsc->gameBG.at(Level3BG), NULL, &rectMainBox);
			SDL_RenderCopy(renderer, L2A1->itterateAnimation(30), NULL, &rectMainBox);
			break;
		case 3: break;
		}

	}
	else if (playA2) {
		switch (levelNumber) {
			case 1: 			SDL_RenderCopy(renderer, rsc->gameBG.at(Level3BG), NULL, &rectMainBox);
								SDL_RenderCopy(renderer, L2A2->itterateAnimation(30), NULL, &rectMainBox);break;
		case 2:
			SDL_RenderCopy(renderer, rsc->gameBG.at(Level3BG), NULL, &rectMainBox);
			SDL_RenderCopy(renderer, L2A2->itterateAnimation(30), NULL, &rectMainBox);
			break;
		case 3: break;
		}

	}
	else if (displayEC) {
		switch (levelNumber) {
			case 1:			SDL_RenderCopy(renderer, rsc->gameBG.at(Level3BG), NULL, &rectMainBox);  break;
		case 2:
			SDL_RenderCopy(renderer, rsc->gameBG.at(Level3BG), NULL, &rectMainBox);
			break;
		case 3: break;
		}
	}

	SDL_RenderPresent(renderer);
}

void MusicRoom::close()
{
	destroyTextLeft();
	destroyTextRight();
}