#include "MessageBox.h"

#include "RescourceKeys.h"
#include "ScreenNames.h"

MessageBox::MessageBox(int* scrn, ResourceMaster* resou, string mainMessage, string subMessage, string yesMessage, string noMessage)
{
	screen = scrn;
	rsc = resou;

	font = new Fonts(0, rsc);
	outlineAnimation = new Animation(rsc->gameAnimations.at(WhiteBlueBG));

	font->loadFont(SDL_Color{ 100,100,100,00 }, yesMessage, 2);
	textYes = font->getTexture();
	font->loadFont(SDL_Color{ 100,100,100,00 }, noMessage, 2);
	textNo = font->getTexture();
	font->loadFont(SDL_Color{ 100,100,100,00 }, mainMessage, 2);
	textMessage = font->getTexture();
	font->loadFont(SDL_Color{ 100,100,100,00 }, subMessage, 2);
	textSubMessage = font->getTexture();

	rectMain = {font->prcnt(0.1, 'x'), font->prcnt(0.1, 'y'), font->prcnt(0.50, 'x'), font->prcnt(0.60, 'y')};
	rectMessage = { font->prcnt(0.15, 'x'), font->prcnt(0.15, 'y'), font->prcnt(0.4, 'x'), font->prcnt(0.2, 'y') };
	rectSubMessage = { font->prcnt(0.20, 'x'), font->prcnt(0.30, 'y'), font->prcnt(0.3, 'x'), font->prcnt(0.15, 'y') };
	rectNo = { font->prcnt(0.15, 'x'), font->prcnt(0.5, 'y'), font->prcnt(0.1, 'x'), font->prcnt(0.1, 'y') };
	rectYes = { font->prcnt(0.30, 'x'), font->prcnt(0.5, 'y'), font->prcnt(0.1, 'x'), font->prcnt(0.1, 'y') };

	boxLoop();
}


MessageBox::~MessageBox()
{
}

void MessageBox::boxLoop()
{
	chrono::milliseconds ms = chrono::duration_cast<chrono::milliseconds>(
		chrono::system_clock::now().time_since_epoch());

	running = true;
	double FPSDelay = 50;

	int initial = 0;
	int expired = 0;

	while (running) {
		ms = chrono::duration_cast<chrono::milliseconds>(
			chrono::system_clock::now().time_since_epoch());
		initial = ms.count();

		processInput();
		update();
		play();
		render();

		while (expired - initial < FPSDelay) {
			ms = chrono::duration_cast<chrono::milliseconds>(
				chrono::system_clock::now().time_since_epoch());
			expired = ms.count();
		}
	}
	close();
}

void MessageBox::processInput()
{
	SDL_Event evnt;
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			running = false;
			*screen = Quit;
			break;
		case SDL_MOUSEMOTION:
			SDL_GetMouseState(&x, &y);
			break;
		case SDL_MOUSEBUTTONDOWN:
			mouseClicked = true;
			break;
		case SDL_MOUSEBUTTONUP:
			mouseClicked = false;
			break;
		}
	}
}

void MessageBox::update() {
	if (x > rectNo.x && x < (rectNo.x + rectNo.w) && y > rectNo.y && y < (rectNo.y + rectNo.h)) {
		cout << "ovrN\n";
		hoverNo = true;
	}
	else if (x > rectYes.x && x < (rectYes.x + rectYes.w) && y > rectYes.y && y < (rectYes.y + rectYes.h)) {
		cout << "ovrY\n";
		hoverYes = true;
	}
	else {
		hoverNo = false;
		hoverYes = false;
	}
	if (!mouseClicked) {
		//block execution
		return;
	}
	else if (hoverNo) {
		response = false;
		running = false;
	}
	else if (hoverYes) {
		response = true;
		running = false;
	}
}

void MessageBox::play() {

}

void MessageBox::render() {

	SDL_RenderCopy(rsc->rendPtr, rsc->gameBG.at(LangBG), NULL, &rectMain);

	if (hoverNo) {
		SDL_RenderCopy(rsc->rendPtr, outlineAnimation->itterateAnimation(30), NULL, &rectNo);
	}
	else if(hoverYes){
		SDL_RenderCopy(rsc->rendPtr, outlineAnimation->itterateAnimation(30), NULL, &rectYes);
	}
	SDL_RenderCopy(rsc->rendPtr, textYes, NULL, &rectYes);
	SDL_RenderCopy(rsc->rendPtr, textNo, NULL, &rectNo);
	SDL_RenderCopy(rsc->rendPtr, textMessage, NULL, &rectMessage);
	SDL_RenderCopy(rsc->rendPtr, textSubMessage, NULL, &rectSubMessage);

	SDL_RenderPresent(rsc->rendPtr);
}

void MessageBox::close() {
	SDL_DestroyTexture(textYes);
	SDL_DestroyTexture(textNo);
	SDL_DestroyTexture(textMessage);
	SDL_DestroyTexture(textSubMessage);

	delete font;
	delete outlineAnimation;
}

bool MessageBox::getResponse() {
	return response;
}