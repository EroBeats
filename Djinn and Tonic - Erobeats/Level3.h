#pragma once
#include "LevelTemplate.h"
class Level3 : public LevelTemplate
{
public:
	Level3(int gameType, FileIO* fileIO, ResourceMaster* resourcePointer, std::array<Animation*, 2> animations);
	~Level3();

	void update();
	void play();
	void render();

	void close();
	void softClose();

	int getScore();
	int getHearts();


	int GT;
	int pumpCounter;
	int playSpeed;

	bool correct;
	bool wrong;
	bool loop;
	bool doneFade;

	bool playAniR;
	bool playAniLGood;
	bool playAniLBad;

	bool playAniPull;
	bool playAniRelease;

	Animation* goodL;
	Animation* badL;

	Animation* pump1R;
	Animation* pump2R;
	Animation* pump3R;

	Animation* pump0R_R;
	Animation* pump1R_R;
	Animation* pump2R_R;
	Animation* pump3R_R;

	Animation* release1;
	Animation* release2;
	Animation* release3;
	Animation* release4;

	SDL_Rect mainBox;
	SDL_Rect rectHeart1;
	SDL_Rect rectHeart2;
	SDL_Rect rectHeart3;
	SDL_Rect rectHeart4;

	SDL_Rect rectScoreTitle;
	SDL_Texture* textScoreTitle;
	SDL_Rect rectScore;
private:
	int score;
	int heartCount;
};