#include "FileIO.h"

#include "ScreenNames.h"
#include "RescourceKeys.h"

FileIO::FileIO()
{
	password = "Test";
}
FileIO::FileIO(FileIO* fio)
{
	rsc = fio->rsc;
	password = "Test";
}
FileIO::FileIO(ResourceMaster* rscrc)
{
	rsc = rscrc;
	password = "Test";
}


FileIO::~FileIO()
{
}

//old methods

void FileIO::storeGameText(std::vector<std::string> item) {
	rsc->gameText = item;
}
//problematic
void FileIO::saveUserData(int data, int line) {
	std::ifstream saveFile("data/save.txt");
	std::ofstream tempFile("data/temp.txt");

	std::array<std::string,3> currentData;
	int counter = 0;
	while (!saveFile.eof()) {
		std::string buffer;
		std::getline(saveFile, buffer);
		if (buffer != "" && buffer != "\n" && buffer != " ") {
			currentData.at(counter) = buffer;
			std::cout << buffer << "\n";
			counter++;
		}
	}

	counter = 0;
	while (counter < 3) {
		if (line != counter + 1) {
			tempFile << currentData.at(counter) + "\n";
		}
		else {
			tempFile << std::to_string(data) + "\n";
		}
		counter++;
	}

	saveFile.close();
	tempFile.close();

	std::remove("data/save.txt");
	std::rename("data/temp.txt", "data/save.txt");
}

void FileIO::saveScoreData(int gamemode, int data, int line) {
	std::ifstream saveFile("data/scores.txt");
	std::ofstream tempFile("data/temp.txt");
	std::array<std::string, 8> currentData;
	int counter = 0;
	while (!saveFile.eof()) {
		std::string buffer;
		std::getline(saveFile, buffer);
		if (buffer != "" && buffer != "\n" && buffer != " ") {
			currentData.at(counter) = buffer;
			counter++;
			std::cout << buffer << "\n";
		}
	}
	std::cout << "skip";
	counter = 0;
	while (counter < 8) {
		if (counter % 2 != gamemode - 1 || counter / 2 + 1 != line) {
			tempFile << currentData.at(counter) + "\n";
		}
		else {
			tempFile << std::to_string(data) + "\n";
		}
		counter++;
	}

	saveFile.close();
	tempFile.close();

	std::remove("data/scores.txt");
	std::rename("data/temp.txt", "data/scores.txt");
}

std::vector<std::string> FileIO::loadLanguage(std::string path)
{
	std::ifstream langData;
	langData.open(path);
	
	std::string text;
	std::vector<std::string> result;
	while (std::getline(langData, text)) {
		std::cout << text << "\n";
		if (!text.empty()) {
			if (text.length() == 1 && text.at(0) != ' ') {
				result.push_back(text);
			}
			else if (text.at(0) != '/' && text.at(1) != '/') {
				result.push_back(text);
			}
		}
	}
	langData.close();
	storeGameText(result);
	return result;
}

int FileIO::loadUserData(int line) {

	std::string storedData;
	std::ifstream data;
	data.open("Data/save.txt");

	std::array<std::string, 3> currentData;
	int counter = 0;
	while (!data.eof()) {
		std::string buffer;
		std::getline(data, buffer);
		if (buffer != "" && buffer != "\n" && buffer != " ") {
			currentData.at(counter) = buffer;
			std::cout << buffer << "\n";
			counter++;
		}
	}
	data.close();

	int intStoredData;
	std::stringstream convert(currentData.at(line - 1));
	if (!(convert >> intStoredData)) {
		intStoredData = -1;
		std::cout << "err\n";
	}
	return intStoredData;
}

int FileIO::loadScoreData(int gamemode, int line) {
	std::string storedData;
	std::ifstream data;
	data.open("Data/scores.txt");
	std::array<std::string, 8> currentData;
	int counter = 0;

	while (!data.eof()) {
		std::string buffer;
		std::getline(data, buffer);
		if (buffer != "" && buffer != "\n" && buffer != " ") {
			currentData.at(counter) = buffer;
			std::cout << buffer;
			counter++;
		}
	}
	std::cout << "\t";

	data.close();

	if (gamemode == arcade) {
		line = (line - 1) * 2 + 1;
	}
	else if (gamemode == life) {
		line = line * 2;
	}

	int intStoredData;
	std::stringstream convert(currentData.at(line - 1));
	if (!(convert >> intStoredData)) {
		intStoredData = -1;
		std::cout << "err\n";
	}
	return intStoredData;
}

/*
void FileIO::loadMenuItems()
{
	std::vector<SDL_Texture*> menuTextures;

	for (int fileName = LangBG; fileName <= GenericBar; fileName++) {
		SDL_Texture* sdlText = loadTexture("Art/" + std::to_string(fileName + 1) + ".png");
		if (sdlText == NULL) {
			std::cout << SDL_GetError() << " Number " << (fileName + 1) << ".png\n";
		}
		menuTextures.push_back(sdlText);
	}
	storeTextures(menuTextures);
}

void FileIO::loadMenuAnimations() {
	std::vector<std::vector<SDL_Texture*>> menuAnimations;

	for (int animationFolder = WhiteBlueBG; animationFolder <= WhiteBlueBG; animationFolder++) {
		std::vector<SDL_Texture*> sdlTextList;
		for (int i = 1; i <= 57; i++) {
			sdlTextList.push_back(loadTexture("Art/" + std::to_string(animationFolder) + "/ (" + std::to_string(i) + ").png"));
		}
		if (sdlTextList.front() == NULL) {
			std::cout << SDL_GetError();
		}
		menuAnimations.push_back(sdlTextList);
	}
	storeAnimations(menuAnimations);
}


void FileIO::loadGameAnimations()
{
	std::vector< std::vector<SDL_Texture*>> gameAnimations;
}

void FileIO::loadGameGraphics()
{
	std::vector<SDL_Texture*> gameGraphics;
}

SDL_Texture* FileIO::loadTexture(std::string path) {

	SDL_Texture* newTexture = NULL;
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());

	newTexture = SDL_CreateTextureFromSurface(rsc->rendPtr, loadedSurface);
	SDL_FreeSurface(loadedSurface);

	return newTexture;
}*/

//new methods

//problematic
SDL_Texture* FileIO::loadSpecificImage(std::string path, int image) {
	return extractSpecificTexture(path, image);
}
std::vector<SDL_Texture*> FileIO::loadSpecificZip(std::string path) {
	return extractTexturesFromZip(path);
}
void FileIO::loadBGZip() {
	rsc->gameBG = extractTexturesFromZip("BG.zip");
}
void FileIO::loadTexturesZip() {
	rsc->gameGraphics = extractTexturesFromZip("Textures.zip");
}
void FileIO::loadAnimationsZip() {
	rsc->gameAnimations.push_back(extractTexturesFromZip("(0).zip"));
	/**/
	for (int i = 1; i <= 36; i++) {	
		rsc->gameAnimations.push_back(extractTexturesFromZip(" (" + std::to_string(i) + ").zip"));
	}
}
void FileIO::loadAnimationsZipUntil(int number) {
	if (number == 0) {
		rsc->gameAnimations.push_back(extractTexturesFromZip("(0).zip"));
	}
	else {
		for (int i = 1; i <= number; i++) {
			rsc->gameAnimations.push_back(extractTexturesFromZip(" (" + std::to_string(i) + ").zip"));
		}
	}
}
void FileIO::loadAnimationsZipPast(int number) {
	if (number == 0) {
		rsc->gameAnimations.push_back(extractTexturesFromZip("(0).zip"));
	}
	for (int i = number + 1; i <= 36; i++) {
		//quick fix for file name issues
		try {
			rsc->gameAnimations.push_back(extractTexturesFromZip("(" + std::to_string(i) + ").zip"));
		}
		catch (std::exception e) {
			rsc->gameAnimations.push_back(extractTexturesFromZip(" (" + std::to_string(i) + ").zip"));
		}
	}
}
void FileIO::loadAnimationsZipInterval(int start, int end) {
	if (start == 0) {
		rsc->gameAnimations.push_back(extractTexturesFromZip("(0).zip"));
		start++;
	}
	for (int i = start; i <= end; i++) {
		//quick fix for file name issues
		try{
			rsc->gameAnimations.push_back(extractTexturesFromZip("(" + std::to_string(i) + ").zip"));
		}
		catch (std::exception e) {
			rsc->gameAnimations.push_back(extractTexturesFromZip(" (" + std::to_string(i) + ").zip"));
		}
	}
}
void FileIO::loadBGMZip() {
	rsc->gameBGM = extractBGMZip("BGM.zip");
}
void FileIO::loadSFXZip() {
	rsc->gameSFX = extractSFXZip("SFX.zip");
}

SDL_Texture* FileIO::extractSpecificTexture(std::string path, int image) {
	unzFile genericZip = unzOpen(path.c_str());
	unz_global_info ugi;
	unzGetGlobalInfo(genericZip, &ugi);

	SDL_Texture* specificText = NULL;

	uLong i;
	std::cout << "Extracting" << "\n";
	for (i = 0; i < ugi.number_entry; i++) {
		unz_file_info ufi;
		char filename[256];
		unzGetCurrentFileInfo(genericZip, &ufi, filename, sizeof(filename), NULL, 0, NULL, 0);
		std::string name = getZipFileName(ufi, genericZip);
		if (name == std::to_string(image)) {
			uInt size_buf = ufi.uncompressed_size;
			void*buf = (void*)malloc(size_buf);
			int err = unzOpenCurrentFilePassword(genericZip, password);
			if (err != UNZ_OK) {
				printf("errPass\n");
			}
			err = unzReadCurrentFile(genericZip, buf, size_buf);
			if (err < 0) {
				printf("ErrReadZip\n");
			}
			SDL_RWops* rw = SDL_RWFromMem(buf, size_buf);
			if (rw == NULL) {
				printf("RW\n");
			}
			SDL_Surface* surf = IMG_Load_RW(rw, 1);
			if (surf == NULL) {
				std::cout << "surf\n";
			}
			specificText = SDL_CreateTextureFromSurface(rsc->rendPtr, surf);
			if (specificText == NULL) {
				std::cout << "text\n";
			}
			SDL_FreeSurface(surf);
			free(buf);
			unzClose(genericZip);
			break;
		}
		unzGoToNextFile(genericZip);
	}
	return specificText;
}

std::vector<Mix_Music*> FileIO::extractBGMZip(std::string path) {
	unzFile bgmZip = unzOpen(path.c_str());
	unz_global_info ugi;
	unzGetGlobalInfo(bgmZip, &ugi);

	std::vector<Mix_Music*> rtnVect(ugi.number_entry);
	std::vector<Mix_Music*>::iterator it;

	SDL_RWops* rw = NULL;
	uLong i;
	std::cout << "Extracting" << "\n";
	for (i = 0; i < ugi.number_entry; i++) {
		unz_file_info ufi;
		char filename[256];
		unzGetCurrentFileInfo(bgmZip, &ufi, filename, sizeof(filename), NULL, 0, NULL, 0);

		uInt size_buf = ufi.uncompressed_size;
		//std::cout << size_buf << "\n";
		void* buf = (void*)malloc(size_buf);
		int err = unzOpenCurrentFilePassword(bgmZip, password);
		if (err != UNZ_OK)
		{
			printf("errPass\n");
		}

		err = unzReadCurrentFile(bgmZip, buf, size_buf);
		if (err < 0) {
			std::cout << "errunzBGM\n";
		}
		rw = SDL_RWFromMem(buf, size_buf);
		if (rw == NULL) {
			std::cout << "RW\n";
		}

		Mix_Music* temp = Mix_LoadMUS_RW(rw, 1);

		std::string name = getZipFileName(ufi, bgmZip);
		char chars[] = "()-.wav";

		for (unsigned int i = 0; i < strlen(chars); ++i)
		{
			// you need include <algorithm> to use general algorithms like std::remove()
			name.erase(std::remove(name.begin(), name.end(), chars[i]), name.end());
		}
		int index = stoi(name) - 1;

		std::cout << index << "\n";

		it = rtnVect.begin() + index;
		rtnVect.at(index) = temp;
		unzGoToNextFile(bgmZip);
	}
	unzClose(bgmZip);
	return rtnVect;
}

std::vector<Mix_Chunk*> FileIO::extractSFXZip(std::string path) {
	unzFile sfxZip = unzOpen(path.c_str());
	unz_global_info ugi;
	unzGetGlobalInfo(sfxZip, &ugi);

	std::vector<Mix_Chunk*> rtrnVector(ugi.number_entry);

	SDL_RWops* rw = NULL;
	uLong i;
	std::cout << "Extracting" << "\n";
	for (i = 0; i < ugi.number_entry; i++) {
		unz_file_info ufi;
		char filename[256];
		unzGetCurrentFileInfo(sfxZip, &ufi, filename, sizeof(filename), NULL, 0, NULL, 0);

		uInt size_buf = ufi.uncompressed_size;
		//std::cout << size_buf << "\n";
		void* buf = (void*)malloc(size_buf);
		int err = unzOpenCurrentFilePassword(sfxZip, password);
		if (err != UNZ_OK)
		{
			printf("errPass\n");
		}

		err = unzReadCurrentFile(sfxZip, buf, size_buf);
		if (err < 0) {
			std::cout << "errunzSFX\n";
		}
		rw = SDL_RWFromMem(buf, size_buf);
		if (rw == NULL) {
			std::cout << "RW\n";
		}
		Mix_Chunk* temp = Mix_LoadWAV_RW(rw, 1);
		std::string name = getZipFileName(ufi, sfxZip);
		char chars[] = "()-.wav";

		for (unsigned int i = 0; i < strlen(chars); ++i)
		{
			// you need include <algorithm> to use general algorithms like std::remove()
			name.erase(std::remove(name.begin(), name.end(), chars[i]), name.end());
		}
		int index = stoi(name) - 1;

		std::cout << index << "\n";
		rtrnVector.at(index) = (temp);

		unzGoToNextFile(sfxZip);
	}
	unzClose(sfxZip);
	return rtrnVector;
}

std::vector<SDL_Texture*> FileIO::extractTexturesFromZip(std::string path) {
	unzFile textureZip = unzOpen(path.c_str());
	unz_global_info ugi;
	unzGetGlobalInfo(textureZip, &ugi);

	std::vector<SDL_Texture*> rtrnVect(ugi.number_entry);

	uLong i;
	std::cout << "Extracting" << "\n";
	for (i = 0; i < ugi.number_entry; i++) {
		unz_file_info ufi;
		char filename[256];
		unzGetCurrentFileInfo(textureZip, &ufi, filename, sizeof(filename), NULL, 0, NULL, 0);

		uInt size_buf = ufi.uncompressed_size;
		//std::cout << size_buf << "\n";
		void* buf = (void*)malloc(size_buf);
		int err = unzOpenCurrentFilePassword(textureZip, password);
		if (err != UNZ_OK)
		{
			printf("errPass\n");
			int err = unzOpenCurrentFilePassword(textureZip, "test");
		}

		err = unzReadCurrentFile(textureZip, buf, size_buf);
		if (err < 0) {
			std::cout << "errunzTxt\n";
		}
		SDL_RWops* rw = SDL_RWFromMem(buf, size_buf);
		if (rw == NULL) {
			std::cout << "RW\n";
		}
		SDL_Surface* surf = IMG_Load_RW(rw, 1);
		if (surf == NULL) {
			std::cout << "surf\n";
		}
		SDL_Texture* text = SDL_CreateTextureFromSurface(rsc->rendPtr, surf);
		if (text == NULL) {
			std::cout << "text\n";
		}

		std::string name = getZipFileName(ufi, textureZip);

		char chars[] = "()-.png";

		for (unsigned int i = 0; i < strlen(chars); ++i)
		{
			// you need include <algorithm> to use general algorithms like std::remove()
			name.erase(std::remove(name.begin(), name.end(), chars[i]), name.end());
		}
		int index = stoi(name) - 1;

		std::cout << index << "\n";

		rtrnVect.at(index) = text;

		SDL_FreeSurface(surf);
		free(buf);
		
		unzGoToNextFile(textureZip);
	}
	unzClose(textureZip);
	return rtrnVect;
}

char* FileIO::getZipFileName(unz_file_info ufi, unzFile uf) {
	char *fileName = (char *)malloc(ufi.size_filename + 1);
	unzGetCurrentFileInfo(uf, &ufi, fileName, ufi.size_filename + 1, NULL, 0, NULL, 0);
	fileName[ufi.size_filename] = '\0';
	return fileName;
}

void FileIO::eraseUserData()
{
		saveUserData(1, LevelUnlocks);
		saveUserData(0, LevelScores);
		saveUserData(0, GameSettings);
	for (int line = 1; line <= 4; line++) {
		std::cout << "skipyyy";
		saveScoreData(arcade, 0, line);
		saveScoreData(life, 0, line);
	}
}

std::vector<int> FileIO::splitInt(std::string str, char delimiter) {
	std::vector<int> internal;
	std::stringstream ss(str);
	std::string result;

	while (std::getline(ss, result, delimiter)) {
		int i = atoi(result.c_str());
		internal.push_back(i);
	}
	return internal;
}
std::vector<std::string> FileIO::split(std::string str, char delimiter) {
	std::vector<std::string> internal;
	std::stringstream ss(str);
	std::string result;

	while (std::getline(ss, result, delimiter)) {
		internal.push_back(result);
	}
	return internal;
}