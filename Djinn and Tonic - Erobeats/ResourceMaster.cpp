#include "ResourceMaster.h"

//HOLD EVERY SINGLE TEXTURE IN CLASSIFIED ARRAYS

ResourceMaster::ResourceMaster()
{
}

ResourceMaster::ResourceMaster(SFX* sfxptr, BGM* bgmptr)
{
	sptr = sfxptr;
	sptr->gameSFX = &gameSFX;
	bptr = bgmptr;
	bptr->gameBGM = &gameBGM;

	SDL_Renderer* rendPtr;
}

ResourceMaster::~ResourceMaster()
{
	for (int i = 0; i < gameGraphics.size(); i++) {
		SDL_DestroyTexture(gameGraphics.at(i));
	}

	for (int i = 0; i < gameAnimations.size(); i++) {
		for (int j = 0; j < (gameAnimations.at(i)).size(); j++) {
			SDL_DestroyTexture(gameAnimations.at(i).at(j));
		}
	}
}

void ResourceMaster::changeResolution(std::string type) {
	if (strcmp(type.c_str(), "1280X1024") == 0) {
		SDL_SetWindowSize(window, 1250, 1000);
		SDL_SetWindowPosition(window, 50, 50);
	}
	else if (strcmp(type.c_str(), "1000X800") == 0) {
		SDL_SetWindowSize(window, 1000, 800);
	}
	else if (strcmp(type.c_str(), "600X480") == 0) {
		SDL_SetWindowSize(window, 640, 480);
	}
}
