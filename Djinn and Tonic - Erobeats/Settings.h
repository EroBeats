#pragma once

#include "ScreenTemplate.h"

#include "FileIO.h"
#include "ResourceMaster.h"
#include "Fonts.h"
#include "Animation.h"
#include "SFX.h"
#include "MessageBox.h"

#include<SDL/SDL.h>
#include <SDL TTF/SDL_ttf .h>
#include <chrono>

using namespace std;
class Settings : ScreenTemplate
{
public:
	Settings();
	Settings(int* screenPointer, FileIO* filePntr, ResourceMaster* resourcePointer);
	~Settings();

	void update();
	void play();
	void render();
	void close();

	void initializeTextures();
	void destroyFonts();

	void reloadLanguage(std::string lang);

	bool hoverLarge;
	bool hoverMedium;
	bool hoverSmall;
	bool hoverClear;
	bool hoverQuit;

	bool hoverSFXBar[4];
	bool hoverBGMBar[4];

	bool hoverEN;
	bool hoverFR;
	bool hoverJP;

	bool playedOnce;
	bool clickPlayedOnce;

	bool* hoverAddress;
	bool* hoverAddressOld;
	
	int clickCooldown;

	int settings;
	int displayNumberSFX;
	int displayNumberBGM;

	Animation* outlineAnimation;

	SDL_Texture* textResolution;
	SDL_Rect rectResolution;

	//SDL_Texture* textLarge;
	//SDL_Rect rectLarge;

	SDL_Texture* textMedium;
	SDL_Rect rectMedium;

	//SDL_Texture* textSmall;
	//SDL_Rect rectSmall;

	SDL_Texture* textBGMLevel;
	SDL_Rect rectBGMLevel;

	SDL_Texture* textBGMIcon;
	SDL_Rect rectBGMIcon;

	SDL_Texture* textSFXLevel;
	SDL_Rect rectSFXLevel;

	SDL_Texture* textSFXIcon;
	SDL_Rect rectSFXIcon;

	SDL_Texture* textBars;
	SDL_Rect SFXBarLocations[4];
	SDL_Rect BGMBarLocations[4];

	SDL_Texture* languageTitle;
	SDL_Rect rectTitleLocation;

	SDL_Texture* languageEN;
	SDL_Rect rectENLocation;

	SDL_Texture* languageFR;
	SDL_Rect rectFRLocation;

	SDL_Texture* languageJP;
	SDL_Rect rectJPLocation;

	SDL_Texture* textQuit;
	SDL_Rect rectQuit;


	SDL_Texture* textClear;
	SDL_Rect rectClear;

	SDL_Rect rectTextSize;
};

