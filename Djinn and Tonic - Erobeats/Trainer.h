#pragma once
#include"TrainerTemplate.h"
#include "LevelTemplate.h"
#include "ScreenTemplate.h"

#include "FileIO.h"
#include "ResourceMaster.h"
#include "Fonts.h"
#include "Animation.h"
#include "SFX.h"
#include "TempoTools.h"

#include <chrono>
#include <SDL\SDL.h>
#include <iomanip>

class Trainer : public LevelTemplate
{
public:
	Trainer();
	Trainer(int* scrn, FileIO* fileIO, ResourceMaster* resourcePointer, int pattern);
	~Trainer();

	void update();
	void render();
	void play();

	Animation* outlineAnimation;

	void createText();
	void destroyText();
	void loadPattern();

	int lightNumber;
	float countDownTime;
	float countDownDuration;

	bool light1Switch;
	bool light2Switch;
	bool light3Switch;
	bool light4Switch;
	bool leftMBSwitch;
	bool rightMBSwitch;

	bool start;
	bool hoverStart;

	bool next;
	bool hoverNext;

	bool perfect;
	bool behind;
	bool ahead;

	int pattern;

	bool isPattern1_1;
	bool isPattern1_2;

	SDL_Texture* goalText;
	SDL_Texture* subGoalText;
	SDL_Texture* levelText;
	SDL_Texture* startText;
	SDL_Texture* nextText;
	SDL_Texture* countDownText;

	SDL_Rect goalRect;
	SDL_Rect subGoalRect;
	SDL_Rect levelRect;
	SDL_Rect startRect;
	SDL_Rect nextRect;
	SDL_Rect countDownRect;

	SDL_Rect aheadRect;
	SDL_Rect behindRect;
	SDL_Rect perfectRect;

	SDL_Rect light1Rect;
	SDL_Rect light2Rect;
	SDL_Rect light3Rect;
	SDL_Rect light4Rect;

	SDL_Rect mouseRect;
	SDL_Rect mouseRightRect;
	SDL_Rect mouseLeftRect;
};

