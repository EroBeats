#include "RhythmIO.h"




RhythmIO::~RhythmIO()
{
}
std::vector<std::vector<int>> RhythmIO::createBeatPatterns(std::string path) {
	return convertBeatMap(readBeatMap(path));
}

std::vector<int> RhythmIO::readBeatMap(std::string path) {
	std::ifstream file(path);
	std::string line;
	getline(file, line);
	std::vector<int> data = splitInt(line, ' ');
	return data;
}

// trim out the zeros and get the times of each
std::vector<std::vector<int>> RhythmIO::convertBeatMap(std::vector<int> data) {
	int count = 0;
	int temp = 0;
	std::vector<std::vector<int>> keys;
	for (int numbers : data) {
		count++;
		if (numbers == 0) {
			continue;
		}
		else if (numbers != 0) {
			keys.push_back(std::vector<int>({numbers, count }));
		}
		else{
			std::cout << " ERROR\n";
		}
		temp++;
		std::cout << keys.at(temp - 1).at(0) << "\t";
	}
	return keys;
}

void RhythmIO::bufferBeatData(vector<vector<int>> keys, long long  musicStart, vector<vector<long long >>* hitData, vector<vector<long long >>* cueData) {
	int keySize = keys.size();
	for (int i = 0; i < 16; i++) {
		if (keyPositions < keySize) {
			if (keys.at(keyPositions).at(0) > 0 && keys.at(keyPositions).at(0) < 10) {
				hitData->push_back(std::vector<long long>({ musicStart + (long long)keys.at(keyPositions).at(1), keys.at(keyPositions).at(0) }));
				std::cout <<  "\nHit Data:\t" << musicStart + (long long)keys.at(keyPositions).at(1) ;
			}
			else if (keys.at(keyPositions).at(0) >= 10) {
				cueData->push_back(std::vector<long long>({ musicStart + (long long)keys.at(keyPositions).at(1), keys.at(keyPositions).at(0) }));
				std::cout << "\nHit Data:\t" << musicStart + (long long)keys.at(keyPositions).at(1);
			}
			keyPositions++;
		}
		else break;
	}
}
