#pragma once

enum gameType { arcade = 1, life = 2 };
enum saveLines { LevelUnlocks = 1, LevelScores = 2, GameSettings = 3 };

enum BGImageKeys {
	LogoBG = 0, LangBG = 1, T1BG = 2, T2BG = 3, T3BG = 4,
	Level3BG = 5
};
enum menuImageKeys{
	//LANG
	LangBGFake = 0, EnFlag = 1, FrFlag = 2, JpFlag = 3,

	//LEVEL SELECT
	GradeF = 4, GradeC = 5, GradeB = 6, GradeA = 7,

	//SETTINGS
	SFXIcon = 8, BGMIcon = 9, GenericBar = 10,

	//TRAINING
	LArrow = 11, RArrow = 12, Circle=13, GreenBright=14, GreenDark=15, RedBright=16, RedDark=17, Mouse=18, MouseL = 19, MouseR = 20,

	//LEVEL
	Heart = 21, GameOver = 22,

	//END CARD
	
};
enum AnimationKeys { WhiteBlueBG = 0, 
	L3Animation1OK = 1, L3Animation1Bad = 2, L3Animation2_1 = 3, L3Animation2_2 = 4, L3Animation2_3 = 5, L3Animation3_1 = 6, L3Animation3_2 = 7, L3Animation3_3 = 8, L3Animation3_4 = 9,
};
enum BGMKeys {
	Song0 = 0, MenuSong = 0, Song3 = 1, Stage3 = 1,
};
enum SFXKeys {
	MenuClickSFX = 0, MenuHoverSFX = 1, LeftSFX = 2, RightSFX = 3, BadSFX = 4, OKSFX = 5, GoodSFX = 6, PerfectSFX = 7, PumpSFX = 8, ReleaseSFX = 9, ErrSFX = 10
};
enum languageKeys {
	//LANG
	ENText = 0, FRText = 1, JPText = 2,

	//MENU
	LevelSelectText = 3, SettingsText = 4, MusicRoomText = 5, QuitText = 6,

	//LEVEL SELECT
	Level1Text = 7, Level2Text = 8, Level3Text = 9, Level4Text = 10, RemixText = 11, BackText = 12,

	//SETTINGS
	ResText = 13, ResLargeText = 14, ResMediumText = 15, ResSmallText = 16, BGMText = 17, SFXText = 18, LanguageText = 19, ClearText = 20,

	//PLAY ROOM
	PRTitleText = 21, Song1Text = 22, Song2Text = 23, Song3Text = 24, Song4Text = 25, Song5Text = 26, Animation1Text = 27, Animation2Text = 28, EndCardText = 29, AzraelText = 30,

	//MESSAGEBOX
	MessageBoxMainText = 31, MessageBoxSubText = 32, MessageBoxNoText = 33, MessageBoxYesText = 34,

	//TRAINER
	GoalText = 35, SubGoalText = 36, NextText = 37, StartText = 38,
	
	//MODE SELECT
	LifeText = 39, ArcadeText = 40, LifeGoalText = 41, ArcadeGoalText = 42,

	//LEVEL
	ScoreText = 43,

	//Loading
	SavingText = 44

};