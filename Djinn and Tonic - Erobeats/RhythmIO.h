#pragma once
#include "FileIO.h"
using namespace std;
class RhythmIO : FileIO
{
public:
	RhythmIO(FileIO* fio) : FileIO(fio) {};
	~RhythmIO();

	int keyPositions;

	std::vector<std::vector<int>> createBeatPatterns(std::string path);
	void bufferBeatData(vector<vector<int>> times, long long musicStart, vector<vector<long long>>* hitData, vector<vector<long long>>* cueData);
private:
	std::vector<int> readBeatMap(std::string filename);
	std::vector<std::vector<int>> convertBeatMap(std::vector<int> data);
};

