﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Language_Editor
{
    public partial class LanguageForm : Form
    {
        string filename = "EN.txt";
        public LanguageForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void english_Click(object sender, EventArgs e)
        {
            filename = "EN.txt";
            userInput.Text = Program.readFile(filename);
        }

        private void french_Click(object sender, EventArgs e)
        {
            filename = "FR.txt";
            userInput.Text = Program.readFile(filename);
        }

        private void japanese_Click(object sender, EventArgs e)
        {
            filename = "JP.txt";
            userInput.Text = Program.readFile(filename);
        }

        private void Save_Click(object sender, EventArgs e)
        {
            Program.writeFile(userInput.Text, filename);
        }
    }
}
