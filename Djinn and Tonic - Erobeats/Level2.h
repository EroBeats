#pragma once
#include "LevelTemplate.h"
class Level2 : public LevelTemplate
{
public:
	Level2();
	~Level2();

	void update();
	void play();
	void render();
};