#pragma once
#include <SDL\SDL.h>

#include<chrono>

#include "ResourceMaster.h"
#include "Fonts.h"
#include "Animation.h"

using namespace std;

class MessageBox
{
public:
	MessageBox(int* scrn, ResourceMaster* resou, string mainMessage, string subMessage, string yesMessage, string noMessage);
	~MessageBox();

	void boxLoop();
	void processInput();
	void update();
	void play();
	void render();
	void close();

	bool getResponse();

	int* screen;
	ResourceMaster* rsc;
	Fonts* font;
	Animation* outlineAnimation;

	int x;
	int y;

	bool running;
	bool mouseClicked;
	bool hoverYes;
	bool hoverNo;

	bool response;

	SDL_Rect rectMain;
	SDL_Rect rectYes;
	SDL_Rect rectNo;
	SDL_Rect rectMessage;
	SDL_Rect rectSubMessage;

	SDL_Texture* textYes;
	SDL_Texture* textNo;
	SDL_Texture* textMessage;
	SDL_Texture* textSubMessage;
};

