#include "LevelTemplate.h"

#include "ScreenNames.h"

//Need a scorebar class

using namespace std::chrono;
LevelTemplate::LevelTemplate()
{
	FPSDelay = 16;
	std::cout << FPSDelay << "\n";

	for (int i = 0; i < 9; i++) {
		playSound[i] = 9;
		playCue[i] = 9;
	}
}

LevelTemplate::~LevelTemplate()
{
}

void LevelTemplate::processInput() {
	if (leftMouseClicked) {
		heldL = true;
	}
	if (rightMouseClicked) {
		heldR = true;
	}
	SDL_Event evnt;
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			running = false;
			*screen = Quit;
			break;
		case SDL_MOUSEMOTION:
			SDL_GetMouseState(&x, &y);
			break;
		case SDL_MOUSEBUTTONDOWN:
			mouseClicked = true;
			if(evnt.button.button == SDL_BUTTON_LEFT){
				leftMouseClicked = true;
			}
			else if (evnt.button.button == SDL_BUTTON_RIGHT) {
				rightMouseClicked = true;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			mouseClicked = false;
			if (evnt.button.button == SDL_BUTTON_LEFT) {
				leftMouseClicked = false;
				heldL = false;
			}
			else if (evnt.button.button == SDL_BUTTON_RIGHT) {
				rightMouseClicked = false;
				heldR = false;
			}
			break;
		}
	}
}

double LevelTemplate::scoreBarPercent(int currScore, int maxScore) {
	return (double)currScore / (double)maxScore;
}

void LevelTemplate::initializeBeats(std::string levelPath) {
	tTools->createTimeData(levelPath);
}
//fadeInDurration = milliseconds
void LevelTemplate::startMusic(int track, int fadeInDuration) {
	rsc->bptr->fadeInBGM(track, fadeInDuration);
	milliseconds ms = duration_cast<std::chrono::milliseconds>(
		system_clock::now().time_since_epoch());
	long long time = ms.count();
	tTools->setMusicStart(time);
}

