#pragma once
#include "LevelTemplate.h"
class TrainerTemplate :
	public LevelTemplate
{
public:
	TrainerTemplate();
	TrainerTemplate(int* scrn, FileIO* fileIO, ResourceMaster* resourcePointer);
	~TrainerTemplate();

	void play();

	Animation* outlineAnimation;

	float countDownTime;
	float countDownDuration;

	SDL_Texture* goalText;
	SDL_Texture* subGoalText;
	SDL_Texture* levelText;
	SDL_Texture* startText;
	SDL_Texture* countDownText;

	SDL_Rect goalRect;
	SDL_Rect subGoalRect;
	SDL_Rect levelRect;
	SDL_Rect startRect;
	SDL_Rect countDownRect;

	SDL_Rect aheadRect;
	SDL_Rect behindRect;
	SDL_Rect perfectRect;

	SDL_Rect light1Rect;
	SDL_Rect light2Rect;
	SDL_Rect light3Rect;

	SDL_Rect mouseRect;
	SDL_Rect mouseRightRect;
	SDL_Rect mouseLeftRect;

};

