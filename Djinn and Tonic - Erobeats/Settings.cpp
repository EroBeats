#include "Settings.h"

#include "ScreenNames.h"
#include "RescourceKeys.h"

Settings::Settings()
{
}

Settings::Settings(int* screenPointer, FileIO* filePntr, ResourceMaster* resourcePointer) {
	
	screen = screenPointer;
	fio = filePntr;
	rsc = resourcePointer;
	renderer = resourcePointer->rendPtr;

	font = new Fonts(0, rsc);
	outlineAnimation = new Animation(rsc->gameAnimations.at(WhiteBlueBG));
	settings = fio->loadUserData(3);

	initializeTextures();

	gameLoop();

}

Settings::~Settings()
{
}

void Settings::destroyFonts() {
	//don't destroy rsc textures
	SDL_DestroyTexture(	 textResolution);
//	SDL_DestroyTexture(	 textLarge);
	SDL_DestroyTexture(	 textMedium);
//	SDL_DestroyTexture(	 textSmall);
	SDL_DestroyTexture(	 textBGMLevel);
	SDL_DestroyTexture(	 textSFXLevel);
	SDL_DestroyTexture(	 languageTitle);
	SDL_DestroyTexture(	 languageEN);
	SDL_DestroyTexture(	 languageFR);
	SDL_DestroyTexture(	 languageFR);
	SDL_DestroyTexture(	 languageJP);
	SDL_DestroyTexture(	 textQuit);
	SDL_DestroyTexture( textClear);
}

void Settings::initializeTextures() {

	destroyFonts();

	int textSizeX = font->prcnt(0.12, 'x');
	int textSizeY = font->prcnt(0.12, 'y');

	int column1 = font->prcnt(0.10, 'x');
	int column2 = font->prcnt(0.30, 'x');
	int column3 = font->prcnt(0.50, 'x');

	int row1 = 0;
	int row2 = font->prcnt(0.12, 'y');
	int row3 = font->prcnt(0.25, 'y');
	int row4 = font->prcnt(0.375, 'y');
	int row5 = font->prcnt(0.50, 'y');
	int row6 = font->prcnt(0.65, 'y');
	int row7 = font->prcnt(0.80, 'y');

	font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(ResText), 2);
	textResolution = font->getTexture();
	rectResolution = { column1, row1, textSizeY * 2, textSizeX  };

	//font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(ResLargeText), 2);
	//textLarge = font->getTexture();
	//rectLarge = { column1, row2 , textSizeY, textSizeX };

	font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(ResMediumText), 2);
	textMedium = font->getTexture();
	rectMedium = { column2, row2 ,  textSizeY * 2, textSizeX };

	//font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(ResSmallText), 2);
	//textSmall = font->getTexture();
	//rectSmall = { column3, row2 , textSizeY, textSizeX };

	font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(SFXText), 2);
	textSFXLevel = font->getTexture();
	rectSFXLevel = { column1, row3 , textSizeY * 2, textSizeX };

	textSFXIcon = rsc->gameGraphics.at(SFXIcon);
	rectSFXIcon = { column1, row4 , textSizeY, textSizeX };

	SDL_SetTextureBlendMode(textBars, SDL_BLENDMODE_BLEND);
	textBars = rsc->gameGraphics.at(GenericBar);

	settings = fio->loadUserData(3);

	//SCREENRES SFXVOL BGMVOL LANGUAGE
	if (settings % 1000 / 100 == 1) {
		displayNumberSFX = 1;
	}
	else if (settings % 1000 / 100 == 2) {
		displayNumberSFX = 2;
	}
	else if (settings % 1000 / 100 == 3) {
		displayNumberSFX = 3;
	}
	else if (settings % 1000 / 100 == 4) {
		displayNumberSFX = 4;
	}
	else {
		displayNumberSFX = 2;
	}

	font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(BGMText), 2);
	textBGMLevel = font->getTexture();
	rectBGMLevel = { column3, row3,  textSizeY * 2, textSizeX };

	textBGMIcon = rsc->gameGraphics.at(BGMIcon);
	rectBGMIcon = { column3, row4,  textSizeY, textSizeX };



	//SCREENRES SFXVOL BGMVOL LANGUAGE
	if (settings % 100 / 10 == 1) {
		displayNumberBGM = 1;
	}
	else if (settings % 100 / 10 == 2) {
		displayNumberBGM = 2;
	}
	else if (settings % 100 / 10 == 3) {
		displayNumberBGM = 3;
	}
	else if (settings % 100 / 10 == 4) {
		displayNumberBGM = 4;
	}
	else {
		displayNumberBGM = 2;
	}

	textBars = rsc->gameGraphics.at(GenericBar);
	SDL_SetTextureBlendMode(textBars, SDL_BLENDMODE_BLEND);
	for (int i = 0; i < 4; i++) {
		BGMBarLocations[i] = { column1 + font->prcnt(0.09, 'x') + 20 * i, row4 + font->prcnt(0.01, 'y'),   font->prcnt(0.01, 'x'), font->prcnt(0.125, 'y') };
	}
	for (int i = 0; i < 4; i++) {
		SFXBarLocations[i] = { column3 + font->prcnt(0.09, 'x') + 20 * i, row4 + font->prcnt(0.01, 'y'),  font->prcnt(0.01, 'x'), font->prcnt(0.125, 'y') };
	}

	font->loadFont(SDL_Color{ 200,200,200,00 }, rsc->gameText.at(LanguageText), 2);
	languageTitle = font->getTexture();
	rectTitleLocation = { column1, row5,  textSizeY * 2, textSizeX };

	font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(ENText), 2);
	languageEN = font->getTexture();
	rectENLocation = { column1, row6,  textSizeY, textSizeX };

	font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(FRText), 2);
	languageFR = font->getTexture();
	rectFRLocation = { column2 , row6,  textSizeY, textSizeX };

	font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(JPText), 2);
	languageJP = font->getTexture();
	rectJPLocation = { column3, row6,  textSizeY, textSizeX };

	font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(BackText), 2);
	textQuit = font->getTexture();
	rectQuit = { column2, row7, textSizeY, textSizeX };

	font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(ClearText), 2);
	textClear = font->getTexture();
	rectClear = { column1, row7, textSizeY, textSizeX };
}


void Settings::update()
{

	hoverLarge = false;
	hoverMedium = false;
	hoverSmall = false;
	hoverQuit = false;
	hoverClear = false;
	hoverEN = false;
	hoverJP = false;
	hoverFR = false;

	for (int i = 0; i < 4; i++) {
		hoverSFXBar[i] = false;
		hoverBGMBar[i] = false;
		if (x > SFXBarLocations[i].x && x < (SFXBarLocations[i].w + SFXBarLocations[i].x) && y > SFXBarLocations[i].y  && y < (SFXBarLocations[i].h + SFXBarLocations[i].y)) {
			////cout << "ovrSFX\n";
			hoverSFXBar[i] = true;
		}
		if (x > BGMBarLocations[i].x && x < (BGMBarLocations[i].w + BGMBarLocations[i].x) && y > BGMBarLocations[i].y  && y < (BGMBarLocations[i].h + BGMBarLocations[i].y)) {
			////cout << "ovrBGM\n";
			hoverBGMBar[i] = true;
		}
	}

	if (x > rectQuit.x && x < (rectQuit.x + rectQuit.w) && y > rectQuit.y && y < (rectQuit.y + rectQuit.h)) {
		////cout << "ovr4\n";
		hoverQuit = true;
	}
	else if (x > rectClear.x && x < (rectClear.x + rectClear.w) && y > rectClear.y && y < (rectClear.y + rectClear.h)) {
		cout << "ovr4\n";
		hoverClear = true;
	}
	else if (x > rectENLocation.x && x < (rectENLocation.x + rectENLocation.w) && y > rectENLocation.y && y < (rectENLocation.y + rectENLocation.h)) {
		//cout << "ovren\n";
		hoverEN = true;
	}
	else if (x > rectFRLocation.x && x < (rectFRLocation.x + rectFRLocation.w) && y > rectFRLocation.y && y < (rectFRLocation.y + rectFRLocation.h)) {
		//cout << "ovrfr\n";
		hoverFR = true;
	}
	else if (x > rectJPLocation.x && x < (rectJPLocation.x + rectJPLocation.w)  && y > rectJPLocation.y && y < (rectJPLocation.y + rectJPLocation.h)) {
		//cout << "ovrjp\n";
		hoverJP = true;
	}
//	else if (x > rectSmall.x && x < (rectSmall.x + rectSmall.w) && y > rectSmall.y && y < (rectSmall.y + rectSmall.h)) {
		//cout << "ovr3\n";
//		hoverSmall = true;
//	}
	else if (x > rectMedium .x && x < (rectMedium .x + rectMedium .w) && y > rectMedium .y && y < (rectMedium .y + rectMedium .h)) {
		//cout << "ovr2\n";
		//hoverMedium = true;
	}
//	else if (x > rectLarge.x && x < (rectLarge.x + rectLarge.w) && y > rectLarge.y && y < (rectLarge.y + rectLarge.h)) {
		//cout << "ovr1\n";
//		hoverLarge = true;
//	}

	int currentTime = SDL_GetTicks();
	if (currentTime - clickCooldown > 500) {
		if (mouseClicked && hoverLarge) {
			//cout << "Fired1\n";
			rsc->changeResolution("1280X1024");
			font->updateWindow();
			initializeTextures();
			int temp = settings / 10000 * 10000;
			settings = temp + settings % 1000 + 3000;
			clickCooldown = SDL_GetTicks();
		}
		else if (mouseClicked && hoverMedium) {
			//cout << "Fired2\n";
			rsc->changeResolution("1000X800");
			font->updateWindow();
			initializeTextures();
			int temp = settings / 10000 * 10000;
			settings = temp + settings % 1000 + 2000;
			clickCooldown = SDL_GetTicks();
		}
		else if (mouseClicked && hoverSmall) {
			//cout << "Fired3\n";
			rsc->changeResolution("600X480");
			font->updateWindow();
			initializeTextures();
			int temp = settings / 10000 * 10000;
			settings = temp + settings % 1000 + 1000;
			clickCooldown = SDL_GetTicks();
		}
		else if (mouseClicked && hoverClear) {
			//cout << "Fired6a\n";

			MessageBox msgBox(screen, rsc, rsc->gameText.at(MessageBoxMainText), rsc->gameText.at(MessageBoxSubText), rsc->gameText.at(MessageBoxNoText), rsc->gameText.at(MessageBoxYesText));
			if (msgBox.getResponse() == true) {
				fio->eraseUserData();
				hoverBGMBar[1] = true;
				hoverSFXBar[1] = true;
			}
			hoverClear = false;
			clickCooldown = SDL_GetTicks();		

		}
		else if (mouseClicked && hoverQuit) {
			//cout << "Fired6b\n";

			//MessageBox msgBox(screen, rsc, rsc->gameText.at(MessageBoxMainText), rsc->gameText.at(MessageBoxNoText), rsc->gameText.at(MessageBoxYesText));
			//if (msgBox.getResponse() == true) {
				fio->saveUserData(settings, 3);
			//}
			running = false;
			*screen = MainMenuScreen;
		}
		else if (mouseClicked && hoverEN) {
			reloadLanguage("EN");
			int temp = settings / 10 * 10;
			settings = temp + settings % 1 + 1;
		}
		else if (mouseClicked && hoverFR) {
			reloadLanguage("FR");
			int temp = settings / 10 * 10;
			settings = temp + settings % 1 + 2;
		}
		else if (mouseClicked && hoverJP) {
			reloadLanguage("JP");
			int temp = settings / 10 * 10;
			settings = temp + settings % 1 + 3;
		}
		for (int i = 0; i < 4; i++) {
			if (mouseClicked &&hoverSFXBar[i]) {
				displayNumberSFX = i + 1;
				rsc->sptr->setVolume(displayNumberSFX);
				int temp = settings / 1000 * 1000;
				settings = temp + settings % 100 + 100 * (i + 1) ;
			}
			if (mouseClicked && hoverBGMBar[i]) {
				displayNumberBGM = i + 1;
				rsc->bptr->setVolume(displayNumberBGM);
				int temp = settings / 100 * 100;
				settings = temp + settings % 10 + 10 * (i + 1);
			}
		}
	}
}

void Settings::play()
{
	if (mouseClicked && (/*hoverLarge || hoverMedium || hoverSmall ||*/ hoverQuit || hoverFR || hoverEN || hoverJP || hoverSFXBar[1] || hoverSFXBar[2] || hoverSFXBar[3] || hoverSFXBar[0]) && !clickPlayedOnce) {
		rsc->sptr->playSFX(0);
		clickPlayedOnce = true;
	}
	else if(!(/*hoverLarge || hoverMedium || hoverSmall || */hoverQuit ||  hoverFR || hoverEN || hoverJP || hoverSFXBar[1] || hoverSFXBar[2] || hoverSFXBar[3] || hoverSFXBar[0])){
		clickPlayedOnce = false;
	}

	if (hoverLarge) {
		hoverAddress = &hoverLarge;
	}
	else if (hoverMedium) {
		hoverAddress = &hoverMedium;
	}
	else if (hoverSmall) {
		hoverAddress = &hoverSmall;
	}
	else if (hoverClear) {
		hoverAddress = &hoverClear;
	}
	else if (hoverQuit) {
		hoverAddress = &hoverQuit;
	}
	else if (hoverFR) { hoverAddress = &hoverFR; }
	else if (hoverEN) { hoverAddress = &hoverEN; }
	else if (hoverJP) { hoverAddress = &hoverJP; }
	
	if (!playedOnce && (/*hoverLarge || hoverMedium || hoverSmall ||*/ hoverQuit || hoverClear || hoverFR || hoverEN || hoverJP ) || hoverAddress != hoverAddressOld) {
		rsc->sptr->playSFX(1);
		playedOnce = true;
	}
	else if (playedOnce && (/*hoverLarge || hoverMedium || hoverSmall || */hoverQuit || hoverClear || hoverFR || hoverEN || hoverJP)) {
		playedOnce = true;
	}
	else {
		playedOnce = false;
	}

	if (hoverLarge) {
		hoverAddressOld = &hoverLarge;
	}
	else if (hoverMedium) {
		hoverAddressOld = &hoverMedium;
	}
	else if (hoverSmall) {
		hoverAddressOld = &hoverSmall;
	}
	else if (hoverQuit) {
		hoverAddressOld = &hoverQuit;
	}
	else if(hoverClear ){ hoverAddressOld = &hoverClear; }
	else if (hoverFR) { hoverAddressOld = &hoverFR; }
	else if (hoverEN) { hoverAddressOld = &hoverEN; }
	else if (hoverJP) { hoverAddressOld = &hoverJP; }
}


void Settings::render()
{
	SDL_RenderClear(renderer);

	SDL_RenderCopy(renderer, rsc->gameBG.at(T2BG), NULL, NULL);

	bool hover1 = false;
	bool hover2 = false;

	if (hoverEN) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectENLocation);
	}
	else if (hoverFR) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectFRLocation);
	}
	else if (hoverJP) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectJPLocation);
	}
	else if (hoverClear) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectClear);
	}
	else if (hoverQuit) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectQuit);
	}
	/*else if (!hover1 && !hover2) {
		if (hoverLarge) {
			//			SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectLarge);
		}
		else if (hoverMedium) {
			SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectMedium);
		}
		else if (hoverSmall) {
			//			SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectSmall);
		}
	}*/

	SDL_RenderCopy(renderer, textResolution, NULL, &rectResolution);

//	SDL_RenderCopy(renderer, textLarge, NULL, &rectLarge);
	SDL_RenderCopy(renderer, textMedium, NULL, &rectMedium);
//	SDL_RenderCopy(renderer, textSmall, NULL, &rectSmall);

	SDL_RenderCopy(renderer, textSFXLevel, NULL, &rectSFXLevel);
	SDL_RenderCopy(renderer, textBGMLevel, NULL, &rectBGMLevel);

	SDL_RenderCopy(renderer, textSFXIcon, NULL, &rectSFXIcon);
	SDL_RenderCopy(renderer, textBGMIcon, NULL, &rectBGMIcon);

	for (int i = 0; i < displayNumberBGM; i++) {
		SDL_SetTextureAlphaMod(textBars, 255);
		SDL_RenderCopy(renderer, textBars, NULL, &BGMBarLocations[i]);
	}
	for (int i = displayNumberBGM; i < 4; i++) {
		SDL_SetTextureAlphaMod(textBars, 100);
		SDL_RenderCopy(renderer, textBars, NULL, &BGMBarLocations[i]);
	}
	for (int i = 0; i < displayNumberSFX; i++) {
		SDL_SetTextureAlphaMod(textBars, 255);
		SDL_RenderCopy(renderer, textBars, NULL, &SFXBarLocations[i]);
	}
	for (int i = displayNumberSFX; i < 4; i++) {
		SDL_SetTextureAlphaMod(textBars, 100);
		SDL_RenderCopy(renderer, textBars, NULL, &SFXBarLocations[i]);
	}

	SDL_SetTextureAlphaMod(outlineAnimation->frames.at(outlineAnimation->currentPosition), 100);
	if (!hover1) {
		for (int i = 0; i < 4; i++) {
			if (hoverBGMBar[i]) {
				SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &BGMBarLocations[i]);
				hover2 = true;
			}
		}
	}
	if (!hover2) {
		for (int i = 0; i < 4; i++) {
			if (hoverSFXBar[i]) {
				SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &SFXBarLocations[i]);
				hover1 = true;
			}
		}
	}
	SDL_SetTextureAlphaMod(outlineAnimation->frames.at(outlineAnimation->currentPosition), 255);

	SDL_RenderCopy(renderer, languageTitle, NULL, &rectTitleLocation);
	SDL_RenderCopy(renderer, languageEN, NULL, &rectENLocation);
	SDL_RenderCopy(renderer, languageFR, NULL, &rectFRLocation);
	SDL_RenderCopy(renderer, languageJP, NULL, &rectJPLocation);

	SDL_RenderCopy(renderer, textClear, NULL, &rectClear);
	SDL_RenderCopy(renderer, textQuit, NULL, &rectQuit);

	SDL_RenderPresent(renderer);
}

void Settings::close()
{
	delete font;
	for (int i = 0; i < outlineAnimation->animationSize; i++) {
		SDL_SetTextureAlphaMod(outlineAnimation->frames.at(i), 255);
		//std::cout << i << " Frame\n";
	}
	delete outlineAnimation;
	destroyFonts();
}

void Settings::reloadLanguage(std::string lang) {
	if(lang == "EN")
		fio->loadLanguage("Language/EN.txt");
	else if (lang == "FR")
		fio->loadLanguage("Language/FR.txt");
	else if (lang == "JP")
		fio->loadLanguage("Language/JP.txt");
	initializeTextures();
}
