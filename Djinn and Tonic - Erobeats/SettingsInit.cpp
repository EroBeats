#include "SettingsInit.h"



SettingsInit::SettingsInit(FileIO* f, ResourceMaster* r)
{
	fio = f;
	rsc = r;

	int settings = fio->loadUserData(3);

	screenRes = (settings / 1000) % 10;
	sfxVol = (settings / 100) % 10;
	bgmVol = (settings / 10) % 10;
	language = (settings / 1) % 10;

	loadLangauges(language);
	setSfxVol(sfxVol);
	setBgmVol(bgmVol);
}


SettingsInit::~SettingsInit()
{
}

void SettingsInit::loadLangauges(int la) {
	switch (la) {
		case 1:	fio->loadLanguage("Language/EN.txt"); break;
		case 2: fio->loadLanguage("Language/FR.txt"); break;
		case 3: fio->loadLanguage("Language/JP.txt"); break;
		default: fio->loadLanguage("Language/EN.txt"); break;
	}
}
void SettingsInit::setSfxVol(int vol) {
	if (vol == 0) {
		rsc->sptr->setVolume(2);
	}
	else {
		rsc->sptr->setVolume(vol);
	}
	std::cout << vol << "SFX\n";
}
void SettingsInit::setBgmVol(int vol) {
	if (vol == 0) {
		rsc->bptr->setVolume(2);
	}
	else {
		rsc->bptr->setVolume(vol);
	}
	std::cout << vol << "BGM\n";
}
int SettingsInit::getResolution() {
	return screenRes;
}
