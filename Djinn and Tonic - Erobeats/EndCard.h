#pragma once
#include "FileIO.h"
#include "ResourceMaster.h"
#include "ScreenTemplate.h"
class EndCard :public ScreenTemplate
{
public:
	EndCard(int* screen, int storeageData, FileIO* fileIO, ResourceMaster* resourcePointer,int gameType, int level);
	~EndCard();

	void update();
	void play();
	void render();
	void close();

	void saveLoadData();

	int GT;
	int storageData;
	int image;

	SDL_Texture* textEndCard;
	SDL_Rect rectEndCard;

	SDL_Texture* textComment;
	SDL_Rect rectComment;

};

