#pragma once
#include "LevelTemplate.h"
class Level5 : public LevelTemplate
{
public:
	Level5();
	~Level5();

	void update();
	void play();
	void render();
};