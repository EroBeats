#pragma once

#include <SDL/SDL.h>
#include <SDL Mixer\SDL_mixer.h>

#include<string>
#include <sstream>
#include <iostream>
#include <fstream>
#include<vector>
#include<array>


#include "unzip.h"

class SFX
{
public:
	SFX();
	~SFX();

	std::vector<Mix_Chunk*>* gameSFX;

	void playSFX(int track);
	void setVolume(int vol);
	bool isFinished(int channel);
};