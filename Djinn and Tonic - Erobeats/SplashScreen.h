#pragma once

#include <iostream>

#include "ResourceMaster.h"

class SplashScreen
{
public:
	SplashScreen(bool* rscLoaded, ResourceMaster* rsc);
	~SplashScreen();

	bool* loaded;
};

