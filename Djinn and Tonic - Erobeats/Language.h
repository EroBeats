#pragma once

#include "ScreenTemplate.h"


#include "FileIO.h"
#include "ResourceMaster.h"
#include "SDLGraphics.h"
#include "Fonts.h"
#include "Animation.h"

#include<chrono>
#include <list>


using namespace std;

class Language : ScreenTemplate
{
public:
	Language(int* screen, FileIO* fileio, ResourceMaster* rscPointer);
	~Language();

	void update();
	void play();
	void render();
	void close();

	void setTextures();
	void destroyFonts();

	int setting;

	bool hoverEN;
	bool hoverFR;
	bool hoverJP;

	bool playedOnce;

	bool* hoverAddress;
	bool* hoverAddressOld;

	SDL_Rect rectFlagEN;
	SDL_Rect largeRectFlagEN;
	SDL_Rect rectFlagFR;
	SDL_Rect largeRectFlagFR;
	SDL_Rect rectFlagJP;
	SDL_Rect largeRectFlagJP;

	SDL_Texture* textEN;
	SDL_Rect rectEN;
	SDL_Texture* textFR;
	SDL_Rect rectFR;
	SDL_Texture* textJP;
	SDL_Rect rectJP;

	Animation *outlineAnimation;
};

