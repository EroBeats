#pragma once
#include "FileIO.h"
#include "ResourceMaster.h"

class SettingsInit
{
public:
	SettingsInit(FileIO* fio, ResourceMaster* rsc);
	~SettingsInit();

	FileIO* fio;
	ResourceMaster* rsc;

	int screenRes;
	int sfxVol;
	int bgmVol;
	int language;

	void loadLangauges(int la);
	void setSfxVol(int vol);
	void setBgmVol(int vol);
	int getResolution();
};

