﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.ComponentModel;
using System.Data;
using System.Drawing;


using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace Level_Editor
{
    class Row : RowGeneral
    {
        int x;
        int y;
        public int[] index = new int[3];

        public Row()
        {
            x = 25;
            y = 21;
        }

        public Row(int[] index)
        {
            x = 25;
            y = 21;
            this.index = index;
        }
        void setY(int Y)
        {
            y = Y;
        }
        void setX(int X)
        {
            x = X;
        }
        public void setLocation()
        {
            Point location = new Point(x,y);
            base.Location = location;
        }
        public void setLocationY(int Y)
        {
            Point location = new Point(x, Y);
            base.Location = location;
        }
        public void setLocationX(int X)
        {
            Point location = new Point(X, y);
            base.Location = location;
        }
        public void setLocation(int X, int Y)
        {
            Point location = new Point(X, Y);
            base.Location = location;
        }
    }
}
