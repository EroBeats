#pragma once
#include <SDL/SDL.h>
#include <SDL Mixer\SDL_mixer.h>

#include<string>
#include <sstream>
#include <iostream>
#include <fstream>
#include<vector>
#include<array>
#include <chrono>
#include <thread>

#include "unzip.h"

class BGM
{
public:
	BGM();
	~BGM();

	std::vector<Mix_Music*>* gameBGM;
	int currentBGM;

	void playBGM();
	void playBGM(int bgmNum);
	void fadeOutBGM(int duration);
	void fadeInBGM(int bgmNumber, int duration);
	void setVolume(int vol);


	std::string password;
};

