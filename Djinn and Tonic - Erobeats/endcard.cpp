#include "EndCard.h"
#include "RescourceKeys.h"
#include "ScreenNames.h"
/*ALL LEVELS SHOULD END UP AT THE END CARD WHICH IS A SERIES OF CONDITIONALS THAT REFLECT BOTH LEVEL AND SCORE
*/

EndCard::EndCard(int* scrn, int stats, FileIO* fileIO, ResourceMaster* resourcePointer, int gameType, int level)
{
	fio = fileIO;
	rsc = resourcePointer;
	renderer = rsc->rendPtr;
	storageData = stats;
	image = level;
	screen = scrn;
	GT = gameType;

	font = new Fonts(0, rsc);
	font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(NextText), 2);
	textComment = font->getTexture();
	rectComment = { font->prcnt(0, 'x'),font->prcnt(0, 'x'),font->prcnt(0.5, 'x'),font->prcnt(0.5, 'x') };

	rectEndCard = { font->prcnt(0, 'x'),font->prcnt(0, 'y'),font->prcnt(1, 'x'),font->prcnt(1, 'y') };

	saveLoadData();

	rsc->bptr->playBGM(MenuSong);
	gameLoop();
}

EndCard::~EndCard()
{
}

void EndCard::update() {
	if (mouseClicked) {
		running = false;
		*screen = LevelSelectScreen;
	}
}
void EndCard::play(){
	if (mouseClicked) {
		rsc->bptr->playBGM(MenuClickSFX);
	}
}
void EndCard::render() {
	SDL_RenderClear(renderer);

	SDL_RenderCopy(renderer, textEndCard, NULL, &rectEndCard); 
	SDL_RenderCopy(renderer, textComment, NULL, &rectComment);

	SDL_RenderPresent(renderer);
}

void EndCard::saveLoadData() {
	SDL_RenderClear(renderer);

	font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(SavingText), 0);
	SDL_Texture* textLoading = font->getTexture();
	SDL_RenderCopy(renderer, textLoading, NULL, &rectEndCard);

	SDL_RenderPresent(renderer);

	SDL_DestroyTexture(textLoading);

	int scoreCap = 100;
	if ((storageData > 0 && GT == 1) || (storageData > scoreCap && GT == 2)) {
		textEndCard = fio->loadSpecificImage("EC.zip", image);
		fio->saveUserData(0 , LevelUnlocks);
		fio->saveUserData(0 , LevelScores);
	}
	else {
		textEndCard = fio->loadSpecificImage("EC.zip", 666);
		fio->saveUserData(0 , LevelScores);
	}

	fio->saveScoreData(GT, storageData, image);

	SDL_Delay(500);
}


void EndCard::close() {
	SDL_DestroyTexture(textEndCard);
	SDL_DestroyTexture(textComment);
	fadeOutMusic(100);
	SDL_Delay(150);
}

