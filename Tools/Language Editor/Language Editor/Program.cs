﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Text;

namespace Language_Editor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            LanguageForm languageForm = new LanguageForm();

            string text = readFile("EN.txt");
            languageForm.userInput.Text = text;

            Application.Run(languageForm);


        }

        public static void writeFile(string text ,string filename)
        {

            StreamWriter sw = new StreamWriter(filename);
            sw.WriteLine(text);
            sw.Flush();
            sw.Close();

            MessageBox.Show("File \"" + filename + "\" was written.");
            Console.Write("Written: " + text);
        }
        public static string readFile(string filename)
        {
            try
            {
                StreamReader sr = new StreamReader(filename);
                string response = "";
                string[] lines = System.IO.File.ReadAllLines(filename);
                foreach(String line in lines)
                {
                    response += line + "\n";    
                }
                sr.Close();
                return response;
            }
            catch
            {
                MessageBox.Show("File not read");
                return "File " + filename + " not read";
            }

            
        }    
    }
}
