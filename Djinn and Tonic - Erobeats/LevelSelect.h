#pragma once

#include "FileIO.h"
#include "ResourceMaster.h"
#include "Fonts.h"
#include "Animation.h"
#include "SFX.h"
#include "ScreenTemplate.h"


#include <chrono>
#include <SDL\SDL.h>

using namespace std;

class LevelSelect : ScreenTemplate
{
public:
	LevelSelect();
	LevelSelect(int* screenPointer, FileIO* filePntr, ResourceMaster* resourcePointer);
	~LevelSelect();

	void update();
	void play();
	void render();
	void close();

	int numUserData;
	int numUserScores;

	bool hoverLevel1;
	bool level1Selectable;
	bool hoverLevel2;
	bool level2Selectable;
	bool hoverLevel3;
	bool level3Selectable;
	bool hoverLevel4;
	bool level4Selectable;
	bool hoverRemix;
	bool remixSelectable;
	bool hoverQuit;
	bool playedOnce;

	bool* hoverAddress;
	bool* hoverAddressOld;

	std::vector<SDL_Texture*> outline;
	Animation* outlineAnimation;

	SDL_Rect boxSize;

	SDL_Texture* textLevel1;
	SDL_Rect rectLevel1;
	SDL_Texture* textLevel1Box;
	SDL_Rect rectLevel1Box;

	SDL_Texture* textLevel2;
	SDL_Rect rectLevel2;
	SDL_Texture* textLevel2Box;
	SDL_Rect rectLevel2Box;

	SDL_Texture* textLevel3;
	SDL_Rect rectLevel3;
	SDL_Texture* textLevel3Box;
	SDL_Rect rectLevel3Box;

	/*SDL_Texture* textLevel4;
	SDL_Rect rectLevel4;
	SDL_Texture* textLevel4Box;
	SDL_Rect rectLevel4Box;
	*/

	/*
	SDL_Texture* textRemix;
	SDL_Rect rectRemix;
	SDL_Texture* textRemixBox;
	SDL_Rect rectRemixBox;
	*/
	SDL_Texture* textQuit;
	SDL_Rect rectQuit;


};


