#include<SDL/SDL.h>

#include "FileIO.h"
#include "ResourceMaster.h"
#include "SDLGraphics.h"


#include "Language.h"
#include "Menu.h"
#include "Settings.h"
#include "LevelSelect.h"
#include "MusicRoom.h"

#include "Trainer.h"
#include "ModeSelect.h"
#include "Level1.h"
#include "Level3.h"
#include "EndCard.h"

#include "ScreenNames.h"

//Go over changes and fix syntax errors
//Add  zips
//Add beatmaps
//Test operations
//soft loss and hard end

/*
SFX and BGM contain pointers to all the sound files and all the ways to work with sound
	These have a password to the storage zips
ResourceMaster contains pointers and origonals for all resources in the game.
	The renderer is created here
	It holds the way to change screen size
FileIO takes stuff out of the storage zips to be located into the rescourcemaster.
	It has a password to the storage zips
	It handles saving and loading of all data
	It has the complex and simple methods of fileIO for zip and raw
SDLGraphics sets up the window and starts loading things with the FileIO class.
	Create load screen creates the load screen after loading all textures into memory
		A new thread loads runs a bg image and the old thread loads the sound files.
		The creation of the thread starts loading the sound files
Inside the main, integers and booleans manage the games life cycle

Language screen loads the languages with flags
Main Menu is the interface
Settigns holds settings
Music room holds animations and sounds
Quit dealocates everything.

*/

int main(int argc, char** argv) {

	SFX* sfxPtr = new SFX();
	BGM* bgmPtr = new BGM();
	ResourceMaster* resourcePointer = new ResourceMaster(sfxPtr, bgmPtr);
	FileIO* fileIO = new FileIO(resourcePointer);

	SDLGraphics* fullInit = new SDLGraphics(fileIO, resourcePointer);
	fullInit->startThread();
	fullInit->createLoadScreen();

	int* screen = new int;
	*screen = LanguageScreen;
	int i = 0;
	std::cout << "lol" << i + 1 << "\n";
	bool runningMain = true;
	while (runningMain) {
		switch(*screen){
			case LanguageScreen: {
				Language lang(screen, fileIO, resourcePointer);
				fileIO->loadAnimationsZipPast(30);
				break;
			}
			case MainMenuScreen: {
				Menu menu(screen, fileIO, resourcePointer);
				break;
			}
			case SettingsScreen: {
				Settings settings(screen, fileIO, resourcePointer);
				break;
			}
			case MusicRoomScreen: {
				MusicRoom musRoom(screen, fileIO, resourcePointer);
				break;
			}
			case Quit: {
				SDL_DestroyRenderer(resourcePointer->rendPtr);
				SDL_DestroyWindow(resourcePointer->window);
				delete resourcePointer;
				delete sfxPtr;
				delete bgmPtr;
				delete fullInit;
				delete screen;
				delete fileIO;
		
				runningMain = false;
				break;
			}

			case LevelSelectScreen: {
				LevelSelect lvlsel(screen, fileIO, resourcePointer);
				break;
			}

			case Level1Screen: {
				ModeSelect mode(screen, fileIO, resourcePointer);
				if (!mode.getSkip()) {
					int gameType = mode.getGameMode();
					Level1 l1;
					if (gameType == 1) {
					}
					else if (gameType == 2) {
					}

					EndCard ec(screen, 0, fileIO, resourcePointer, gameType, 1);
				}
				break;
			}
			case Level3Screen: {
				ModeSelect mode(screen, fileIO, resourcePointer);
				if (!mode.getSkip()) {
					int gameType = mode.getGameMode();
					Level3 l3(gameType, fileIO, resourcePointer, mode.getAnimations());
					if (gameType == 1) {
						int score = l3.getScore();
						EndCard ec(screen, score, fileIO, resourcePointer, gameType, 3);
					}
					else if (gameType == 2) {
						int hearts = l3.getHearts();
						EndCard ec(screen, hearts, fileIO, resourcePointer, gameType, 3);
					}
				}
				break;
			}
		}
	}
	return 0;
}