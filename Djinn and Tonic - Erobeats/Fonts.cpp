#include "Fonts.h"

Fonts::Fonts() {

}

Fonts::Fonts(int fontType, ResourceMaster* rescources){
	TTF_Init();
	switch (fontType) {
	case 0:
		font = TTF_OpenFont("Fonts/Roboto-Black.ttf", 78);
		break;
	case 1:
		font = TTF_OpenFont("Fonts/Cirno.ttf", 78);
		break;
	}

	rsc = rescources;
	
	renderer = rsc->rendPtr;
	
	SDL_GetWindowSize(rsc->window, &windowWidth, &windowHeight);
}


Fonts::Fonts(int fontType, ResourceMaster* rsc, SDL_Color color, std::string words, int rendering)
{
	//
	TTF_Init();
	switch (fontType) {
	case 0:
		font = TTF_OpenFont("Fonts/Roboto-Black.ttf", 28);
		break;
	}

	loadFont(color, words, rendering);

	renderer = rsc->rendPtr;
	SDL_GetWindowSize(rsc->window, &windowWidth, &windowHeight);
}


Fonts::~Fonts()
{
}

void Fonts::changeColor() {

}

void Fonts::loadFont(SDL_Color color, std::string words, int rendering) {
	SDL_Surface* textSurface;
	//3 for outline, 2 for high, 1 for BG 0 for low, default low
	if (rendering == 0) {
		textSurface = TTF_RenderText_Solid(font, words.c_str(), color);
	}
	else if (rendering == 1) {
		textSurface = TTF_RenderText_Shaded(font, words.c_str(), color, SDL_Color{ 255,255,255 });
	}
	else if (rendering == 2) {
		textSurface = TTF_RenderText_Blended(font, words.c_str(), color);
	}
	else if (rendering == 3) {
		TTF_SetFontOutline(font, 1);
		textSurface = TTF_RenderText_Blended(font, words.c_str(), color);
		TTF_SetFontOutline(font, 0);
	}
	else {
		textSurface = TTF_RenderText_Solid(font, words.c_str(), color);
	}



	if (textSurface == NULL) {
		std::cout << TTF_GetError() <<"\n";
	}
	fontTexture = SDL_CreateTextureFromSurface(renderer, textSurface);

	SDL_FreeSurface(textSurface);
}

int Fonts::prcnt(double location, char axis) {
	switch (axis) {
	case 'x':
		return location * windowWidth;
	case 'y': 
		return location * windowHeight;
	default: 
		return 0;
	}
}


void Fonts::updateWindow() {
	SDL_GetWindowSize(rsc->window, &windowWidth, &windowHeight);
}

SDL_Texture* Fonts::getTexture() {
	return fontTexture;
}

SDL_Rect* Fonts::getPosition() {
	return location;
}
