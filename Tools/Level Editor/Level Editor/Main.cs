﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using Microsoft.VisualBasic;

using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System.Windows.Input;

namespace Level_Editor
{
    
    public partial class Main : Form
    {
        ArrayList rowSet = new ArrayList();
        int [] beatPositions;
        double frames = 1000;

        //int fps = 60;

        double numberOfBeats;
        double smallestInterval;
        double numberOfRows;
        double duration;
        double tempo;
        string lowest;
        string timeSig;
        int divisions;
        int beatsPerLine;
        int splits;


        public Main()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e){}
        private void label2_Click(object sender, EventArgs e) {}
        private void label4_Click(object sender, EventArgs e){}
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e){}
        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e){}
        private void textBox1_TextChanged(object sender, EventArgs e){}

        private void Generate_Click(object sender, EventArgs e)
        {
            duration = double.Parse(length.Text);
            tempo = double.Parse(bpm.Text);
            lowest = division.Text;
            timeSig = timeSignature.Text;
            divisions = 0;
            beatsPerLine = 0;
            splits = 0;

            switch (lowest)
            {
                case "Quarter": divisions = 1; break;
                case "Eigth": divisions = 2; break;
                case "Triplet": divisions = 3; break;
                case "Sixteenth": divisions = 4; break;
                case "Sextuplet": divisions = 6; break;
                case "ThirtySecond": divisions = 8; break;
                case "SixtyFourth": divisions = 16; break;
            }
            switch (timeSig)
            {
                case "4/4":
                    beatsPerLine = 16;
                    splits = 4;
                    break;
                case "3/4": beatsPerLine = 18;
                    splits = 3;
                    break;
                case "6/8":
                    beatsPerLine = 18;
                    splits = 6;
                    break;
            }
            double secInMin = 60.0;
            numberOfBeats = (tempo / secInMin) * duration;
            smallestInterval = (secInMin / tempo ) * (1.0 / divisions);
            numberOfRows = numberOfBeats / beatsPerLine;

            Console.WriteLine(smallestInterval);

            int counter = 0;
            double rowBeats = beatsPerLine;

            int arraySize = (int)(duration * frames);
            reset();
            beatPositions = new int[arraySize];

            while (numberOfRows > 0)
            {
                Row row = new Row();
                int xSize = (int)(row.Width / beatsPerLine);
                if (numberOfRows < 1)
                {
                    row.Width = (int)(numberOfRows * row.Width + 5);
                }
                row.setLocationY(21 + counter * 100);
                content.Controls.Add(row);

                int bars = 0;
                for (int i = 0; i < rowBeats; i++)
                {
                    Row beat = new Row();
                    beat.setColor(Color.Gray);
                    if (numberOfRows < 1)
                    {
                        rowBeats = (int)numberOfBeats;
                    }

                    beat.setWidth(xSize - 5);
                    beat.setLocation(i * xSize + 5, 0);
                    row.Controls.Add(beat);

                    if (bars++ % splits == 0)
                    {
                        Panel split = new Panel();
                        split.SetBounds(i * xSize, 0, 10, 69);
                        split.BackColor = Color.Black;
                        row.Controls.Add(split);
                    }

                    for (int j = 0; j < divisions; j++)
                    {
                        Row subDivision = new Row(new int[]{counter, i, j });
                        subDivision.BackColor = Color.Red;
                        int width = beat.Width / divisions - 1;
                        subDivision.Width = width;
                        subDivision.Height = beat.Height;
                        subDivision.Location = new Point(j * (width + 2), 0);

                        subDivision.MouseDown += addToArray;
                        beat.Controls.Add(subDivision);

                    }
                }
                rowSet.Add(row);
                numberOfRows -= 1.0;
                numberOfBeats -= 16;
                counter++;
            }
        }

        public void addToArray(object sender, EventArgs e)
        {
            Row row = (Row)sender;
            if ((MouseButtons.ToString()).Equals("Left"))
            {
                int data = int.Parse(dataType.Text.ToString());
                if (0 == data)
                {
                    row.BackColor = Color.Red;
                }
                else if (1 == data)
                {
                    row.BackColor = Color.Orange;
                }
                else if (2 == data)
                {
                    row.BackColor = Color.Yellow;
                }
                else if (3 == data)
                {
                    row.BackColor = Color.Green;
                }
                else if (4 == data)
                {
                    row.BackColor = Color.Cyan;
                }
                else if (5 == data)
                {
                    row.BackColor = Color.Blue;
                }
                else if (6 == data)
                {
                    row.BackColor = Color.Purple;
                }
                else if (7 == data)
                {
                    row.BackColor = Color.Pink;
                }
                else if (8 == data)
                {        
                    row.BackColor = Color.DarkRed;
                }
                else if (9 == data)
                {
                    row.BackColor = Color.OrangeRed;
                }
                else if (10 == data)
                {     
                    row.BackColor = Color.Black;
                }
                else if (20 == data)
                {
                    row.BackColor = Color.DarkGray;
                }
                else if (30 == data)
                {
                    row.BackColor = Color.DarkSlateGray;
                }
                else if (40 == data)
                {
                    row.BackColor = Color.DimGray;
                }
                else if (50 == data)
                {
                    row.BackColor = Color.Gray;
                }
                else if (60 == data)
                {
                    row.BackColor = Color.LightGray;
                }
                else if (70 == data)
                {
                    row.BackColor = Color.LightSlateGray;
                }
                else if (80 == data)
                {
                    row.BackColor = Color.SlateGray;
                }
                else if (90 == data)
                {
                    row.BackColor = Color.White;
                }
                addToArray(row.index, data);
            }
            else if ((MouseButtons.ToString()).Equals("Middle")) {
                row.BackColor = Color.Black;
                addToArray(row.index, 10);
            }
            else if ((MouseButtons.ToString()).Equals("Right"))
            {              
                row.BackColor = Color.Red;
                addToArray(row.index, 0);
            }         
        }
        public void addToArray(int[] location, int data)
        {
            int space = beatsPerLine * divisions * location[0] + divisions * location[1] + location[2];
            double time = smallestInterval * space;  
            int index = (int)(Math.Round(time / (1.0 / frames)));
            Console.WriteLine(location[0] + " " + location[1] + " " + location[2] + " = " + space);
            Console.WriteLine(beatsPerLine + "bpl " + divisions + "Div " + smallestInterval + "intervals\n" + 1.0 / frames + " Sec/Frame");
            Console.WriteLine(time);
            Console.WriteLine(index + "/" + beatPositions.Length + "\n");
         
            beatPositions[index] = data;
        }

        private void Save_Click(object sender, EventArgs e)
        {
            string file = fileName.Text;
            if (beatPositions == null)
            {
                MessageBox.Show("No Data Entered", "File");
            }
            else if(file == "")
            {
                MessageBox.Show("Empty name", "File");
            }
            else
            {

                MessageBox.Show("File " + file + " was created", "File");
                StreamWriter sw = new StreamWriter(file);
                foreach(int integer in beatPositions)
                {
                    sw.Write(integer + " ");
                }
                sw.Write(beatPositions);
                sw.Flush();
                sw.Close();
            }
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This will clear everything", "Reset");
            reset();
        }

        public void reset()
        {
            foreach (Row row in rowSet)
            {
                row.Dispose();
                beatPositions = null;
            }
        }




        private void content_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Main_Load(object sender, EventArgs e)
        {
            //read from root
            //if no file say so, otherwise load it
            //split line by blanks
            //each blank gets run through addToArray();
        }

        private void division_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void vScrollBar1_Scroll_1(object sender, ScrollEventArgs e)
        {

        }
    }
}
