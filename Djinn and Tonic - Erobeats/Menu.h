#pragma once

#include "ScreenTemplate.h"

#include "FileIO.h"
#include "ResourceMaster.h"
#include "Fonts.h"
#include "Animation.h"
#include "SFX.h"
#include "BGM.h"

#include<SDL\SDL.h>
#include<iostream>
#include<chrono>
using namespace std;
class Menu : ScreenTemplate
{
public:
	Menu(int* scrs, FileIO* filePntr, ResourceMaster* resourcePointer);
	~Menu();

	void update();
	void play();
	void render();
	void close();

	void destroyFonts();

	bool hoverLevelSel;
	bool hoversettings;
	bool hovermusicRoom;
	bool hoverQuit;
	bool playedOnce;

	bool* hoverAddress;
	bool* hoverAddressOld;

	Animation *outlineAnimation;

	SDL_Rect rectTextSize;

	SDL_Texture* textLevelSelect;
	SDL_Rect rectLevelSelect;

	SDL_Texture* textSettings;
	SDL_Rect rectSettings;

	SDL_Texture* textMusicRoom;
	SDL_Rect rectMusicRoom;

	SDL_Texture* textQuit;
	SDL_Rect rectQuit;

};

