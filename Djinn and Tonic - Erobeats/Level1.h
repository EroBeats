#pragma once
#include "LevelTemplate.h"

//Level1 is a rhythm game mode that involves rhythmically reacting to cues.
//It only involves left and right clicks without sustain or anything else fancy like that.
//It is just a matter of keeping a beat and hearing when to click.

class Level1 : public LevelTemplate
{
public:
	Level1();
	~Level1();

	void update();
	void play();
	void render();
};

