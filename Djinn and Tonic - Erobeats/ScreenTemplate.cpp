#include "ScreenTemplate.h"

#include "ScreenNames.h"

using namespace std;

ScreenTemplate::ScreenTemplate()
{
	FPSDelay = 32;
	alpha = 255;
	fadeRate = 0;
	std::cout << FPSDelay << "\n";
}


ScreenTemplate::~ScreenTemplate()
{
}

void ScreenTemplate::gameLoop() {
	chrono::milliseconds ms = chrono::duration_cast<chrono::milliseconds>(
		chrono::system_clock::now().time_since_epoch());

	running = true;
	long long initial = 0;
	long long expired = 0;

	while (running) {
		ms = chrono::duration_cast<chrono::milliseconds>(
			chrono::system_clock::now().time_since_epoch());
		initial = ms.count();

		processInput();
		update();
		play();
		render();

		while (expired - initial < FPSDelay) {
			ms = chrono::duration_cast<chrono::milliseconds>(
				chrono::system_clock::now().time_since_epoch());
			expired = ms.count();

		}
	}
	close();
}

void ScreenTemplate::processInput() {
	SDL_Event evnt;
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			running = false;
			*screen = Quit;
			break;
		case SDL_MOUSEMOTION:
			SDL_GetMouseState(&x, &y);
			break;
		case SDL_MOUSEBUTTONDOWN:
			mouseClicked = true;
			if (evnt.button.button == SDL_BUTTON_LEFT) {
				mouseClickedL = true;
				//std::cout << "L\n";
			}
			else {
				mouseClickedR = true;
			}
			break;

		case SDL_MOUSEBUTTONUP:
			mouseClicked = false;
			mouseClickedL = false;
			mouseClickedR = false;
			break;
		}
	}
}
bool ScreenTemplate::fadeIn(double durration, SDL_Texture* fadeTexture) {
	bool done;
	if (alpha < 255) {
		if (fadeRate == 0) fadeRate = 255 / (durration / (1 / FPSDelay));
		alpha += fadeRate;
		SDL_SetTextureAlphaMod(fadeTexture, alpha);
		done = false;		
	}
	else {
		fadeRate = 0;
		done = true;
	}
	return done;
}

bool ScreenTemplate::fadeOut(double durration, SDL_Texture* fadeTexture) {
	bool done;
	if (alpha > 0) {
		if (fadeRate == 0) fadeRate = 255 / (durration / (1 / FPSDelay));
		alpha -= fadeRate;
		SDL_SetTextureAlphaMod(fadeTexture, alpha);
		done = false;
	}
	else {
		fadeRate = 0;
		done = true;
	}
	return done;
}

//fadeOutDurration = milliseconds
void ScreenTemplate::fadeOutMusic(int fadeOutDuration) {
	rsc->bptr->fadeOutBGM(fadeOutDuration);
	int fadeInDuration;
}

void ScreenTemplate::update() {
}
void ScreenTemplate::play() {
}
void ScreenTemplate::render() {
}
void ScreenTemplate::close(){
}
