#include "TempoTools.h"

using namespace std::chrono;

TempoTools::TempoTools(RhythmIO* rhythmio)
{
	rio = rhythmio;

	errorMarginPerf = 16;
	errorMarginGood = 32;
	errorMarginOK = 48;
	errorMarginBad = 64;

	long long dummyTime = (duration_cast<milliseconds>(system_clock::now().time_since_epoch())).count();
	for (int i = 0; i < 10; i++) {
		oldTime[i] = dummyTime;
		fileTime[i] = dummyTime;
	}
}

TempoTools::~TempoTools()
{
}

void TempoTools::createTimeData(std::string levelPath) {
	clearTimeData();
	timeKeys = rio->createBeatPatterns(levelPath);
}
void TempoTools::clearTimeData() {
	timeKeys.clear();
	hitData.clear();
	cueData.clear();
	rio->keyPositions = 0;
}
void TempoTools::proccessDataSlow() {
	rio->bufferBeatData(timeKeys, musicStart, &hitData, &cueData);
}
void TempoTools::setMusicStart(long long mStart) {
	musicStart = mStart;
}

void TempoTools::countDown(float* time, float duration) {
	milliseconds ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch());
	long long expiredTime = ms.count();
	*time = duration - (expiredTime - musicStart) / 1000.0;
}

double TempoTools::clickTempo(int time) {
	int limit = 3;
	clickTimes.push_back(time);
	int size = clickTimes.size();

	if (size == 1) {
		return 1;
	}
	else if (size > limit) {
		clickTimes.erase(clickTimes.begin());
		size = limit;
	}
	double combined = 0;
	for (int t = 0; t < size - 1; t++) {
		combined += clickTimes.at(t + 1) - clickTimes.at(t);
	}
	//time seperation
	double combinedAv = combined / (size - 1);
	//CPM
	double clickRate = 60000 / combinedAv;
	double factor = clickRate / tempo;
	if (combinedAv < 1) {
		return 1;
	}
	else {
		return factor;
	}

}

short TempoTools::autoPlay(int type){
	milliseconds ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch());
	long long time = ms.count();
	long long  difference = 0;
	for (int i = 1; i < 10; i++) {
		if (type == i * 10) {
			difference = cueFileTime[i] - time;
			//if(type == 10)	std::cout << (difference) << "\n";
			if (difference < 16 && difference > -16 && !autoplayed[i]) {
				autoplayed[i] = true;
				return 1;
			}
			else if (difference < 16 && difference > -16 && autoplayed[i]) {}
			else {
				autoplayed[i] = false;
			}
		}
	}
	return 0;
}

short TempoTools::clickAccuracy(int type, bool cout) {
	milliseconds ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch());

	long long clickTime = ms.count();
	short hit;
	long long difference;
	long long oldDifference;
	for (int i = 1; i < 10; i++) {
		if (type == i) {
			//Ahead
			difference = std::abs(fileTime[i] - clickTime);
			//Behind
			oldDifference = std::abs(oldTime[i] - clickTime);
		}
	}
	if (cout) {
		std::cout << "\n ----\t" << (oldDifference) << " ---- \n"
			<< "\n ----\t" << difference << " ---- \n";
	}

	if (difference < errorMarginOK || oldDifference < errorMarginOK) {
		if (cout) std::cout << "OK\n";
		return 0;
	}
	else if (difference > errorMarginOK){
		if (cout)	std::cout << "Ahead\n";
		return 1;
		//ahead
	}
	else if (oldDifference > errorMarginOK) {
		if (cout)std::cout << "Behind\n";
		return -1;
		//behind
	}
	else return 9;
}
short TempoTools::clickAccuracyRange(int type) {
	milliseconds ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch());

	long long clickTime = ms.count();
	short hit;
	long long difference;
	long long oldDifference;
	for (int i = 1; i < 10; i++) {
		if (type == i) {
			//Ahead
			difference = std::abs(fileTime[i] - clickTime);
			//Behind
			oldDifference = std::abs(oldTime[i] - clickTime);
		}
	}
	std::cout << "\n ----\t" << (oldDifference) << " ---- \n"
		<< "\n ----\t" << difference << " ---- \n";
	//signs are flipped for some reason.
	if (difference < errorMarginPerf || oldDifference < errorMarginPerf) {
		std::cout << "A";
		return 0;
	}
	else if (difference < errorMarginGood || oldDifference < errorMarginGood) {
		std::cout << "B";
		return 1;
		//ahead
	}
	else if (difference < errorMarginOK || oldDifference < errorMarginOK) {
		std::cout << "C";
		return 2;
		//ahead
	}
	else if (difference > errorMarginOK || oldDifference > errorMarginOK) {
		std::cout << "F";
		return 3;
		//ahead
	}
	else return 9;
}

//when a certain vector's time has expired, switch to the next one and keep the old one in a long long.
void TempoTools::moveTicker() {
	milliseconds ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch());
	long long ct = ms.count();

	for (int i = 1; i < 10; i++) {
		if (hitData.size() != 0) {
			if (ct > fileTime[i] && hitData.at(0).at(1) == i) {
				oldTime[i] = fileTime[i];
				fileTime[i] = hitData.at(0).at(0);
				hitData.erase(hitData.begin());
				std::cout << "\tupdate Hit " << hitData.size() << "\n";
			}
		}
	}
	for (int i = 1; i < 10; i++) {
		if (cueData.size() != 0) {
			if (ct > cueFileTime[i] && cueData.at(0).at(1) == i * 10) {
				cueFileTime[i] = cueData.at(0).at(0);
				cueData.erase(cueData.begin());
				std::cout << "\tupdate Cue" << cueData.size() << "\n";
			}
		}
		else {
			done = true;
		}
	}
}