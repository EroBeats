#pragma once

#include "ScreenTemplate.h"

#include "FileIO.h"
#include "ResourceMaster.h"
#include "Fonts.h"
#include "Animation.h"
#include "SFX.h"
#include "BGM.h"


#include <chrono>
#include <SDL\SDL.h>
#include <SDL Mixer\sdl_mixer.h>

//modify into the play room

using namespace std;
class MusicRoom : ScreenTemplate
{
public:
	MusicRoom();
	MusicRoom(int* screenPointer, FileIO* filePntr, ResourceMaster* resourcePntr);
	~MusicRoom();

	void update();
	void play();
	void render();
	void close();

	void destroyTextLeft();
	void generateTextLeft();
	void destroyTextRight();
	void generateTextRight(int level);

	int numUserData;
	int numUserScores;
	int levelNumber;

	bool hoverLevel1;
	bool level1Selectable;
	bool hoverLevel2;
	bool level2Selectable;
	bool hoverLevel3;
	bool level3Selectable;
	bool hoverBGM;
	bool BGMSelectable;
	bool hoverA1;
	bool A1Selectable;
	bool hoverA2;
	bool A2Selectable;
	bool hoverEC;
	bool ECSelectable;
	bool hoverQuit;
	bool playedOnce;

	bool playA1;
	bool playA2;
	bool displayEC;

	bool* hoverAddress;
	bool* hoverAddressOld;

	std::vector<SDL_Texture*> outline;

	Animation* outlineAnimation;
	Animation* L2A1;
	Animation* L2A2;

	SDL_Texture* textLevel1;
	SDL_Rect rectLevel1;

	SDL_Texture* textLevel2;
	SDL_Rect rectLevel2;

	SDL_Texture* textLevel3;
	SDL_Rect rectLevel3;

	SDL_Texture* textBGM;
	SDL_Rect rectBGM;

	SDL_Texture* textA1;
	SDL_Rect rectA1;

	SDL_Texture* textA2;
	SDL_Rect rectA2;

	SDL_Texture* textEC;
	SDL_Rect rectEC;

	SDL_Texture* textQuit;
	SDL_Rect rectQuit;
	
	SDL_Rect rectMainBox;
	SDL_Rect rectSelectionBox;

};

