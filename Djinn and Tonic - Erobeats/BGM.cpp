#include "BGM.h"

enum BGMKeys {MenuTheme = 0};

BGM::BGM()
{
	//Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4000);

	password = "Test";
}


BGM::~BGM()
{
	for (int i = 0; i < gameBGM->size(); i++) {
		Mix_FreeMusic(gameBGM->at(i));
	}

	Mix_Quit(); 
}

void BGM::playBGM() {
	Mix_PlayMusic(gameBGM->at(0), -1);
	currentBGM = 0;
}
/**/
void BGM::playBGM(int bgmNumber) {
	Mix_PlayMusic(gameBGM->at(bgmNumber), -1);
	currentBGM = bgmNumber;
}
//Similar to playBGM, but it fades in
void BGM::fadeInBGM(int bgmNumber, int duration) {
	Mix_FadeInMusic(gameBGM->at(currentBGM), -1, duration);
	currentBGM = bgmNumber;
}
void BGM::fadeOutBGM(int duration) {
	Mix_FadeOutMusic(duration);
}
/**/
void BGM::setVolume(int level) {
	std::cout << Mix_VolumeMusic(MIX_MAX_VOLUME *  level / 4) << " - BGM Volume Level\n";
}


