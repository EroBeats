#pragma once
#include "LevelTemplate.h"
class Level4 : public LevelTemplate
{
public:
	Level4();
	~Level4();

	void update();
	void play();
	void render();
};