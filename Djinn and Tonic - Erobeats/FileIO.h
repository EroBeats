#pragma once

#include<string>
#include <sstream>
#include <iostream>
#include <fstream>
#include<vector>
#include<array>

#include <SDL\SDL.h>
#include <SDL Image\sdl_image.h>

#include "unzip.h"
#include <ZLIB\zlib.h>

#include "ResourceMaster.h"

class FileIO
{
public:
	
	FileIO(ResourceMaster* rsc);
	FileIO(FileIO* fio);
	FileIO();
	~FileIO();

	ResourceMaster* rsc;

	void storeGameText(std::vector<std::string> item);

	/*
	void storeTextures(std::vector<SDL_Texture*> item);
	void storeAnimations(std::vector<std::vector<SDL_Texture*>> item);	
	*/

	void saveUserData(int data, int line);
	void saveScoreData(int gamemode, int data, int line);

	int loadUserData(int line);
	int loadScoreData(int gamemode, int line);
	std::vector<std::string>loadLanguage(std::string path);


	//Lazy
	/*void loadMenuItems();
	void loadMenuAnimations();
	void loadGameGraphics();
	void loadGameAnimations();*/

	//Complex
	SDL_Texture* loadSpecificImage(std::string path, int image);
	std::vector<SDL_Texture*> loadSpecificZip(std::string path);
	void loadBGZip();
	void loadTexturesZip();
	void loadAnimationsZip();
	void loadAnimationsZipUntil(int number);
	void loadAnimationsZipPast(int number);
	void loadAnimationsZipInterval(int start, int end);
	void loadSFXZip();
	void loadBGMZip();

	char* getZipFileName(unz_file_info ufi, unzFile uf);

	void eraseUserData();

	std::vector<std::string> split(std::string str, char delimiter);
	std::vector<int> splitInt(std::string str, char delimiter);

	//SDL_Texture* extractSpecificTexture(std::string path, int image);
private:
	//SDL_Texture* loadTexture(std::string path);
	SDL_Texture* extractSpecificTexture(std::string path, int image);
	std::vector<SDL_Texture*> extractTexturesFromZip(std::string path);
	std::vector<Mix_Chunk*> extractSFXZip(std::string path);
	std::vector<Mix_Music*> extractBGMZip(std::string path);

	char* password;
};
