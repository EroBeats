﻿namespace Level_Editor
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.length = new System.Windows.Forms.TextBox();
            this.bpm = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Save = new System.Windows.Forms.Button();
            this.Reset = new System.Windows.Forms.Button();
            this.generate = new System.Windows.Forms.Button();
            this.tools = new System.Windows.Forms.Panel();
            this.load = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.timeSignature = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.fileName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.division = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.content = new System.Windows.Forms.Panel();
            this.tools.SuspendLayout();
            this.SuspendLayout();
            // 
            // length
            // 
            this.length.Location = new System.Drawing.Point(33, 48);
            this.length.Name = "length";
            this.length.Size = new System.Drawing.Size(100, 20);
            this.length.TabIndex = 0;
            this.length.Text = "120";
            this.length.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // bpm
            // 
            this.bpm.Location = new System.Drawing.Point(33, 112);
            this.bpm.Name = "bpm";
            this.bpm.Size = new System.Drawing.Size(100, 20);
            this.bpm.TabIndex = 2;
            this.bpm.Text = "120";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Length(Seconds)";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "BPM";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // dataType
            // 
            this.dataType.FormattingEnabled = true;
            this.dataType.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "10",
            "20",
            "30",
            "40"});
            this.dataType.Location = new System.Drawing.Point(33, 302);
            this.dataType.Name = "dataType";
            this.dataType.Size = new System.Drawing.Size(100, 21);
            this.dataType.TabIndex = 5;
            this.dataType.Text = "1";
            this.dataType.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Time Signature";
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(43, 485);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(75, 23);
            this.Save.TabIndex = 7;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(43, 562);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(75, 23);
            this.Reset.TabIndex = 8;
            this.Reset.Text = "Reset";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // generate
            // 
            this.generate.Location = new System.Drawing.Point(43, 441);
            this.generate.Name = "generate";
            this.generate.Size = new System.Drawing.Size(75, 23);
            this.generate.TabIndex = 12;
            this.generate.Text = "Generate";
            this.generate.UseVisualStyleBackColor = true;
            this.generate.Click += new System.EventHandler(this.Generate_Click);
            // 
            // tools
            // 
            this.tools.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tools.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.tools.Controls.Add(this.load);
            this.tools.Controls.Add(this.splitter1);
            this.tools.Controls.Add(this.timeSignature);
            this.tools.Controls.Add(this.label6);
            this.tools.Controls.Add(this.fileName);
            this.tools.Controls.Add(this.label5);
            this.tools.Controls.Add(this.generate);
            this.tools.Controls.Add(this.division);
            this.tools.Controls.Add(this.label4);
            this.tools.Controls.Add(this.dataType);
            this.tools.Controls.Add(this.Reset);
            this.tools.Controls.Add(this.length);
            this.tools.Controls.Add(this.Save);
            this.tools.Controls.Add(this.bpm);
            this.tools.Controls.Add(this.label3);
            this.tools.Controls.Add(this.label1);
            this.tools.Controls.Add(this.label2);
            this.tools.Location = new System.Drawing.Point(911, 0);
            this.tools.Name = "tools";
            this.tools.Size = new System.Drawing.Size(151, 677);
            this.tools.TabIndex = 9;
            // 
            // load
            // 
            this.load.Location = new System.Drawing.Point(43, 524);
            this.load.Name = "load";
            this.load.Size = new System.Drawing.Size(75, 23);
            this.load.TabIndex = 18;
            this.load.Text = "Load";
            this.load.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 677);
            this.splitter1.TabIndex = 17;
            this.splitter1.TabStop = false;
            // 
            // timeSignature
            // 
            this.timeSignature.FormattingEnabled = true;
            this.timeSignature.Items.AddRange(new object[] {
            "4/4",
            "3/4",
            "6/8"});
            this.timeSignature.Location = new System.Drawing.Point(32, 231);
            this.timeSignature.Name = "timeSignature";
            this.timeSignature.Size = new System.Drawing.Size(100, 21);
            this.timeSignature.TabIndex = 16;
            this.timeSignature.Text = "4/4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 268);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Data Stored";
            // 
            // fileName
            // 
            this.fileName.Location = new System.Drawing.Point(33, 364);
            this.fileName.Name = "fileName";
            this.fileName.Size = new System.Drawing.Size(100, 20);
            this.fileName.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 340);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "File Name";
            // 
            // division
            // 
            this.division.FormattingEnabled = true;
            this.division.Items.AddRange(new object[] {
            "Quarter",
            "Triplet",
            "Eigth",
            "Sixteenth",
            "Sextuplet",
            "ThirtySecond",
            "SixtyFourth"});
            this.division.Location = new System.Drawing.Point(33, 174);
            this.division.Name = "division";
            this.division.Size = new System.Drawing.Size(100, 21);
            this.division.TabIndex = 11;
            this.division.Text = "Quarter";
            this.division.SelectedIndexChanged += new System.EventHandler(this.division_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Lowest Division";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // content
            // 
            this.content.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.content.AutoScroll = true;
            this.content.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.content.Location = new System.Drawing.Point(-1, -1);
            this.content.Name = "content";
            this.content.Size = new System.Drawing.Size(909, 678);
            this.content.TabIndex = 10;
            this.content.Paint += new System.Windows.Forms.PaintEventHandler(this.content_Paint);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 679);
            this.Controls.Add(this.content);
            this.Controls.Add(this.tools);
            this.Name = "Main";
            this.Text = "Level Editor 62.5FPS";
            this.Load += new System.EventHandler(this.Main_Load);
            this.tools.ResumeLayout(false);
            this.tools.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox length;
        private System.Windows.Forms.TextBox bpm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox dataType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Panel tools;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel content;
        private System.Windows.Forms.ComboBox division;
        private System.Windows.Forms.Button generate;
        private System.Windows.Forms.TextBox fileName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox timeSignature;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Button load;
    }
}

