#pragma once

#include<SDL\SDL.h>
#include <SDL TTF\SDL_ttf .h>
#include<string>

#include<iostream>

#include "ResourceMaster.h"

class Fonts
{
public:
	Fonts();
	Fonts(int fontType, ResourceMaster* rsc);
	Fonts(int fontType, ResourceMaster* rsc, SDL_Color color, std::string words, int rendering);
	~Fonts();

	void changeColor();
	void loadFont(SDL_Color color, std::string words, int rendering);
	int prcnt(double objectLocation, char axis);
	void updateWindow();

	SDL_Texture* getTexture();
	SDL_Rect* getPosition();


	ResourceMaster* rsc;
	TTF_Font *font;
	SDL_Texture *fontTexture;
	SDL_Rect* location;
	SDL_Renderer* renderer;
	int windowWidth;
	int windowHeight;

};

