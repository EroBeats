﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Data;
using System.Drawing;


using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace Level_Editor
{
    class RowGeneral : Panel
    {
        public RowGeneral()
        {
            base.BackColor = Color.White;
            base.Width = 1725;
            base.Height = 69;
        }
        public double getWidth()
        {
            return base.Width;
        }
        public void setWidth(int w)
        {
            base.Width = w;
        }
        public void setColor(Color c)
        {
            base.BackColor = c;
        }
    }
}
