#include "ModeSelect.h"

#include "RescourceKeys.h"
#include "ScreenNames.h"

ModeSelect::ModeSelect(int* scrs, FileIO* fileIO, ResourceMaster* resourcePointer)
{
	hoverArcade = false;
	hoverLife = false;
	hoverStart = false;

	hoverAddress = false;
	hoverAddressOld = false;

	rsc = resourcePointer;
	fio = fileIO;

	renderer = rsc->rendPtr;
	screen = scrs;

	outlineAnimation = new Animation(rsc->gameAnimations.at(WhiteBlueBG));
	animations[0] = new Animation(fio->loadSpecificZip("1_0.zip"));
	animations[1] = new Animation(fio->loadSpecificZip("1_1.zip"));
	font = new Fonts(0, rsc);

	std::string hs = std::to_string(fio->loadScoreData(1, *screen % 50));	
	if (hs == "0") {
		highscore = "Highscore: ----";
	}
	else {
		highscore = "Highscore: " + hs;
	}

	std::string hl = std::to_string(fio->loadScoreData(2, *screen % 50));
	if (hl == "0") {
		highlife = "Highlife: ----";
	}
	else {
		highlife = "Highlife: " + hl;
	}

	createFonts();
	gameLoop();
}


ModeSelect::~ModeSelect()
{
}

int ModeSelect::getGameMode() {
	return gameMode;
}


void ModeSelect::createFonts() {
	int textSizeX = font->prcnt(0.20, 'x');
	int textSizeY = font->prcnt(0.15, 'y');
	int textAlignX = font->prcnt(0.10, 'x');
	int offsetAlignX = font->prcnt(0.17, 'x');
	int centerAlignX = font->prcnt(0.45, 'x');

	if (gameMode == 0) {
		font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(GoalText), 2);
	}
	else if (gameMode == 1) {
		font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(ArcadeText), 2);
	}
	else if (gameMode == 2) {
		font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(LifeText), 2);
	}

	textGoal = font->getTexture();
	rectGoal = { textAlignX, font->prcnt(0.03, 'y'), textSizeX , textSizeY };


	if (gameMode == 0) {
		font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(SubGoalText), 2);
		rectSubGoal = { offsetAlignX , font->prcnt(0.14, 'y'), textSizeX , textSizeY };
	}
	else if (gameMode == 1) {
		font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(ArcadeGoalText), 2);
		rectSubGoal = { offsetAlignX, font->prcnt(0.14, 'y'), textSizeX * 4 , textSizeY };
	}
	else if (gameMode == 2) {
		font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(LifeGoalText), 2);
		rectSubGoal = { offsetAlignX, font->prcnt(0.14, 'y'), textSizeX * 3 , textSizeY };
	}
	textSubGoal = font->getTexture();

	font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(ArcadeText), 2);
	textArcade = font->getTexture();
	rectArcade = { textAlignX, font->prcnt(0.27, 'y'), textSizeX , textSizeY };

	font->loadFont(SDL_Color{ 100,100,100,00 }, highscore, 2);
	textHighScore = font->getTexture();
	rectHighScore = { offsetAlignX, font->prcnt(0.37, 'y'), textSizeX , textSizeY };

	font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(LifeText), 2);
	textLife = font->getTexture();
	rectLife = { textAlignX, font->prcnt(0.50, 'y'), textSizeX , textSizeY };

	font->loadFont(SDL_Color{ 100,100,100,00 }, highlife, 2);
	textHighLife = font->getTexture();
	rectHighLife = { offsetAlignX, font->prcnt(0.60, 'y'), textSizeX , textSizeY };

	if (gameMode == 0) {
		font->loadFont(SDL_Color{ 100,100,100,00 }, rsc->gameText.at(StartText), 2);
	}
	else {
		font->loadFont(SDL_Color{ 255,255,255,00 }, rsc->gameText.at(StartText), 2);
	}
	textStart = font->getTexture();
	rectStart = { centerAlignX, font->prcnt(0.78, 'y'), font->prcnt(0.266, 'x'), font->prcnt(0.20, 'y') };

}

void ModeSelect::destroyFonts() {
	SDL_DestroyTexture(textArcade);

	SDL_DestroyTexture(textHighScore);

	SDL_DestroyTexture(textLife);

	SDL_DestroyTexture(textHighLife);

	SDL_DestroyTexture(textGoal);

	SDL_DestroyTexture(textSubGoal);

	SDL_DestroyTexture(textStart);
}


void ModeSelect::update() {
	hoverArcade = false;
	hoverLife = false;
	hoverStart = false;

	if (modeSelected && x > rectStart.x && x < rectStart.x + rectStart.w && y > rectStart.y && y < rectStart.y + rectStart.h) {
		hoverStart = true;
	}
	else if (x > rectLife.x && x < rectLife.x + rectLife.w && y > rectLife.y && y < rectLife.y + rectLife.h) {
		hoverLife = true;
	}
	else if (x > rectArcade.x && x < rectArcade.x + rectArcade.w && y > rectArcade.y && y < rectArcade.y + rectArcade.h) {
		hoverArcade = true;
	}

	if (mouseClicked && hoverStart && modeSelected) {
		running = false;
	}
	else if (mouseClicked && hoverArcade) {
		modeSelected = true;
		gameMode = 1;
		destroyFonts();
		createFonts();
	}
	else if (mouseClicked && hoverLife) {
		modeSelected = true;
		gameMode = 2;
		destroyFonts();
		createFonts();
	}

	if (mouseClickedR) {
		running = false;
		*screen = LevelSelectScreen;
	}
}
void ModeSelect::play() {
	if (hoverStart) {
		hoverAddress = &hoverStart;
	}
	if (hoverLife) {
		hoverAddress = &hoverLife;
	}
	if (hoverArcade) {
		hoverAddress = &hoverArcade;
	}
	if (mouseClicked && (hoverStart || hoverArcade || hoverLife) && !playedOnceL) {
		rsc->sptr->playSFX(0);
		playedOnceL = true;
	}
	else if (mouseClicked && playedOnceL) {
		playedOnceL = true;
	}
	else{
		playedOnceL = false;
	}
	if (!playedOnce && (hoverStart|| hoverArcade || hoverLife) || hoverAddress != hoverAddressOld) {
		rsc->sptr->playSFX(1);
		playedOnce = true;
	}
	else if (playedOnce && (hoverStart || hoverArcade || hoverLife)) {
		playedOnce = true;
	}
	else {
		playedOnce = false;
	}
	hoverAddressOld = hoverAddress;
}
void ModeSelect::render() {
	SDL_RenderClear(renderer);

	SDL_RenderCopy(renderer, rsc->gameGraphics.at(LangBGFake), NULL, NULL);

	if (hoverStart) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectStart);
	}
	else if (hoverLife) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectLife);
	}
	else if (hoverArcade) {
		SDL_RenderCopy(renderer, outlineAnimation->itterateAnimation(30), NULL, &rectArcade);
	}

	SDL_RenderCopy(renderer, textArcade, NULL, &rectArcade);
	SDL_RenderCopy(renderer, textHighScore, NULL, &rectHighScore);
	SDL_RenderCopy(renderer, textLife, NULL, &rectLife);
	SDL_RenderCopy(renderer, textHighLife, NULL, &rectHighLife);
	SDL_RenderCopy(renderer, textStart, NULL, &rectStart);
	SDL_RenderCopy(renderer, textGoal, NULL, &rectGoal);
	SDL_RenderCopy(renderer, textSubGoal, NULL, &rectSubGoal);

	SDL_RenderPresent(renderer);
}

void ModeSelect::close() {
	destroyFonts();
	if (!mouseClickedR) {
		fadeOutMusic(500);
		SDL_Delay(500);
	}
	else {
		animations.at(1)->destroyAnimation();
		animations.at(0)->destroyAnimation();

		skip = true;
	}
}
std::array<Animation*, 2> ModeSelect::getAnimations() {
	return animations;
}
 
bool ModeSelect::getSkip() {
	return skip;
}

